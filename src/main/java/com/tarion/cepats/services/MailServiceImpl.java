package com.tarion.cepats.services;

import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.web.dtos.UserDto;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.StringWriter;
import java.util.Date;


/**
 *
 * @author oagady
 * @date Oct 17, 2014
 * @version 1.0
 *
 */
@Service
public class MailServiceImpl implements MailService{

	private static final String DATE_TODAY = "dateToday";
	private static final String NAME = "name";
	private static final String ENROLMENT_NUMBER = "enrolmentNumber";
	private static final String UPDATES = "updates";
	private static final String ERRORS = "errors";
	private static final String UTF_8 = "UTF-8";
	private static final String PHONE_NUMBER = "phoneNumber";
	private static final String BODY_INITIAL_UPLOAD_SECOND_YEAR = "bodyInitialUploadSecondYear.vm";
	private static final String SUBJECT_INITIAL_UPLOAD_SECOND_YEAR = "subjectInitialUploadSecondYear.vm";
	private static final String BODY_INITIAL_UPLOAD_SECOND_YEAR_VB = "bodyInitialUploadSecondYearVB.vm";
	private static final String BODY_INITIAL_UPLOAD_FIRST_YEAR = "bodyInitialUploadFirstYear.vm";
	private static final String BODY_INITIAL_UPLOAD_FIRST_YEAR_VB = "bodyInitialUploadFirstYearVB.vm";
	private static final String SUBJECT_INITIAL_UPLOAD_FIRST_YEAR = "subjectInitialUploadFirstYear.vm";
	private static final String BODY_FORM_UPDATES_SECOND_YEAR = "bodyFormUpdatesSecondYear.vm";
	private static final String SUBJECT_FORM_UPDATES_SECOND_YEAR = "subjectFormUpdatesSecondYear.vm";
	private static final String BODY_FORM_UPDATES_FIRST_YEAR = "bodyFormUpdatesFirstYear.vm";
	private static final String SUBJECT_FORM_UPDATES_FIRST_YEAR = "subjectFormUpdatesFirstYear.vm";
	private static final String BODY_UPLOAD_ERRORS = "bodyUploadErrors.vm";
	private static final String SUBJECT_UPLOAD_ERROR = "subjectUploadError.vm";

    private MailSender mailSender;
    private PropertyService propertyService;
    private VelocityEngine velocityEngine;

    @Autowired
    public MailServiceImpl(MailSender mailSender, PropertyService propertyService, VelocityEngine velocityEngine){
        this.mailSender = mailSender;
        this.propertyService = propertyService;
        this.velocityEngine = velocityEngine;
    }
    
    public void sendMailInitialUploadFirstYear(String toAddress, String name, String enrolmentNumber, boolean isVendorBuilder){
    	long startTime = LoggerUtil.logEnter(MailServiceImpl.class, "ENTER - sendMailInitialUploadFirstYear {} {}",enrolmentNumber,  toAddress);
        VelocityContext velocityContext = getVelocityContext(name, enrolmentNumber);
    	String emailBodyTemplate = isVendorBuilder ? BODY_INITIAL_UPLOAD_FIRST_YEAR_VB : BODY_INITIAL_UPLOAD_FIRST_YEAR;
        sendTextMail(toAddress, SUBJECT_INITIAL_UPLOAD_FIRST_YEAR, emailBodyTemplate, velocityContext);
		LoggerUtil.logExit(MailServiceImpl.class, "sendMailInitialUploadFirstYear", startTime);
    }

    public void sendMailInitialUploadSecondYear(String toAddress, String name, String enrolmentNumber, boolean isVendorBuilder){
    	long startTime = LoggerUtil.logEnter(MailServiceImpl.class, "ENTER - sendMailInitialUploadSecondYear {} {}", enrolmentNumber, toAddress);
        VelocityContext velocityContext = getVelocityContext(name, enrolmentNumber);
    	String emailBodyTemplate = isVendorBuilder ? BODY_INITIAL_UPLOAD_SECOND_YEAR_VB : BODY_INITIAL_UPLOAD_SECOND_YEAR;
        sendTextMail(toAddress, SUBJECT_INITIAL_UPLOAD_SECOND_YEAR, emailBodyTemplate, velocityContext);
		LoggerUtil.logExit(MailServiceImpl.class, "sendMailInitialUploadSecondYear", startTime);
    }

    @Override
    public void sendUploadError(String enrolmentNumber, FormTypeEnum formType, String uploadSummary, String emailAddress, UserDto user) {
    	long startTime = LoggerUtil.logEnter(MailServiceImpl.class, "ENTER - sendUploadError {} {}", enrolmentNumber, emailAddress);

        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put(ERRORS, uploadSummary);

        sendTextMail(emailAddress, SUBJECT_UPLOAD_ERROR, BODY_UPLOAD_ERRORS, velocityContext);
		LoggerUtil.logExit(MailServiceImpl.class, "sendUploadError", startTime);
    }
       
    @Override
    public void sendFormUpdateFirstYear(String emailBody, String emailAddress, String name, String enrolmentNumber) {
    	long startTime = LoggerUtil.logEnter(MailServiceImpl.class, "ENTER - sendFormUpdateFirstYear {} {}", enrolmentNumber, emailAddress);

        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put(UPDATES, emailBody);

        sendTextMail(emailAddress, SUBJECT_FORM_UPDATES_FIRST_YEAR, BODY_FORM_UPDATES_FIRST_YEAR, velocityContext);
		LoggerUtil.logExit(MailServiceImpl.class, "sendFormUpdateFirstYear", startTime);
    }
    
    @Override
    public void sendFormUpdateSecondYear(String emailBody, String emailAddress, String name, String enrolmentNumber) {
    	long startTime = LoggerUtil.logEnter(MailServiceImpl.class, "ENTER - sendFormUpdateSecondYear {} {}", enrolmentNumber, emailAddress);

        VelocityContext model = getVelocityContext(name, enrolmentNumber);
        model.put(UPDATES, emailBody);

        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put(UPDATES, emailBody);

        sendTextMail(emailAddress, SUBJECT_FORM_UPDATES_SECOND_YEAR, BODY_FORM_UPDATES_SECOND_YEAR, velocityContext);
		LoggerUtil.logExit(MailServiceImpl.class, "sendFormUpdateSecondYear", startTime);
    }

	private void sendTextMail(String toAddress, String subjectTemplate, String bodyTemplate, VelocityContext context) {
		long start = LoggerUtil.logEnter(MailServiceImpl.class, "sendTextMail {} {} {} {} ", toAddress, subjectTemplate, bodyTemplate, context);

		try {
            StringWriter subjectWriter = new StringWriter();
            velocityEngine.mergeTemplate(subjectTemplate, UTF_8, context, subjectWriter);
            String subject = subjectWriter.toString();

            StringWriter bodyWriter = new StringWriter();
            velocityEngine.mergeTemplate(subjectTemplate, UTF_8, context, bodyWriter);
            String body = subjectWriter.toString();

            mailSender.sendEmailUsingEsp(subject, body, null, toAddress);
            LoggerUtil.logExit(MailServiceImpl.class, "sendTextMail", start);
        } catch (Exception e) {
            LoggerUtil.logError(MailServiceImpl.class, "Failed to send mail");
        }

    }

    private VelocityContext getVelocityContext(UserDto userDto){
        VelocityContext velocityContext = getVelocityContext(CEPATSUtil.getFirstNameLastName(userDto), userDto.getEnrolment().getEnrolmentNumber());
        populateDateAndPhoneNumber(velocityContext);
        return velocityContext;
    }

    private VelocityContext getVelocityContext(String name, String enrolmentNumber){
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put(NAME, name);
        velocityContext.put(ENROLMENT_NUMBER, enrolmentNumber);
        populateDateAndPhoneNumber(velocityContext);
        return velocityContext;
    }

	private void populateDateAndPhoneNumber(VelocityContext velocityContext) {
        velocityContext.put(DATE_TODAY, CEPATSUtil.convertDateToString(new Date(System.currentTimeMillis()), null));
        velocityContext.put(PHONE_NUMBER, propertyService.getInquiresPhoneNumber());
	}
}
