package com.tarion.cepats.web.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.services.ContentService;
import com.tarion.cepats.web.dtos.ContentDto;

@RequestMapping("/faq")
@Controller
public class ContentController {

	@Autowired
	ContentService contentService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "text/html")
	public String getFAQ(Model model, HttpServletRequest request, HttpSession session) {
		List<ContentDto> dtos = contentService.findByContentType("faq");
		model.addAttribute("faqs", dtos);
		
		model.addAttribute("headerCssClass", "myhomeHeader");
		
		return "faq";
	}
	
}