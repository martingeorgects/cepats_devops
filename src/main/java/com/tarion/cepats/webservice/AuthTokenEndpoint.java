package com.tarion.cepats.webservice;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import cepats.auth_token.GetAuthTokenRequest;
import cepats.auth_token.GetAuthTokenResponse;

import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.entities.SsoLoginEntity;
import com.tarion.cepats.services.SSOService;

@Endpoint
public class AuthTokenEndpoint {

	private static final String NAMESPACE_URI = "http://cepats/auth.token.webservice";

	@Autowired
	SSOService ssoService;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAuthTokenRequest")
	@ResponsePayload
	public GetAuthTokenResponse getAuthToken(
			@RequestPayload GetAuthTokenRequest request) {
		long startTime = LoggerUtil.logEnter(AuthTokenEndpoint.class, "getAuthToken"
				, "Received getAuthTokenRequest: userid: {}, firstName: {}, lastName: {}, sourceSystem: {}, enrolmentNumber: {}, email: {}, , userType: {}",
				request.getUserId(), request.getFirstName(),
				request.getLastName(), request.getSourceSystem(),
				request.getEnrolmentNumber(), request.getEmailAddress(),
				request.getUserType());
		
		String authToken = RandomStringUtils.randomAlphanumeric(16);
		
		//verify there is no existing entry with this token yet
		SsoLoginEntity existingUser = ssoService.loadUserByToken(authToken);
		while(existingUser != null) {
			authToken = RandomStringUtils.randomAlphanumeric(16);
			existingUser = ssoService.loadUserByToken(authToken);
		}
		ssoService.saveUser(authToken, request.getUserId(),
				request.getFirstName(), request.getLastName(),
				request.getSourceSystem(), request.getEnrolmentNumber(),
				request.getEmailAddress(), request.getUserType());

		GetAuthTokenResponse response = new GetAuthTokenResponse();
		response.setToken(authToken);
		LoggerUtil.logExit(AuthTokenEndpoint.class, "getAuthToken", authToken, startTime);
		return response;
	}
}
