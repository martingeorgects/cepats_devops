package com.tarion.cepats.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.services.EmailNotificationService;
import com.tarion.cepats.services.MailSender;
import com.tarion.cepats.services.MailService;
import com.tarion.cepats.services.VersionHistoryService;

@RequestMapping("/remote")
@Controller
public class RemoteController {

   @Autowired
    private MailSender mailSender;
   
   @Autowired
   private MailService mailService;
   
   @Autowired
   private VersionHistoryService historyService;
   
   @Autowired
   EmailNotificationService notificationService;
    

	
	@RequestMapping(value = "/notify/{numberOfDays}", method = RequestMethod.GET)
	public void notifyWeekly(@PathVariable("numberOfDays") String numberOfDays, HttpServletRequest request) {
		LoggerUtil.logDebug(RemoteController.class, "notifyWeekly - client ip", request.getRemoteAddr());
		
		int numberOfDaysInt = Integer.parseInt(numberOfDays);

		try {
			notificationService.forceNotifications(numberOfDaysInt);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}


}