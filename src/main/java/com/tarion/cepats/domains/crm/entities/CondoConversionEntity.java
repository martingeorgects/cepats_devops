package com.tarion.cepats.domains.crm.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "PS_RD_SITE")
public class CondoConversionEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SITE_ID")
	private String enrolmentNumber;
	
	@Column(name = "TWC_CONDO_CONV_FLG")
	private String condoConversionFlag;

	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

	public String getCondoConversionFlag() {
		return condoConversionFlag;
	}

	public void setCondoConversionFlag(String condoConversionFlag) {
		this.condoConversionFlag = condoConversionFlag;
	}


}
