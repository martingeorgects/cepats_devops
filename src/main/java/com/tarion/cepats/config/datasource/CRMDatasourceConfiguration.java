package com.tarion.cepats.config.datasource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;

@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "crmEntityManagerFactory",
        basePackages = { "com.tarion.cepats.domains.crm" }
)
public class CRMDatasourceConfiguration {

    @Bean(name = "crmEntityManagerFactory")
    public LocalEntityManagerFactoryBean entityManagerFactory(){
        LocalEntityManagerFactoryBean factoryBean = new LocalEntityManagerFactoryBean();
        factoryBean.setPersistenceUnitName("crm");
        return factoryBean;
    }

}
