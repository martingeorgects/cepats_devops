package com.tarion.cepats.validators.impl;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.validators.Priority;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Aug 28, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 *
 * Implementation of validating priority field in the form item
 * to be used with @Priority custom annotation
 *
 */

public class PriorityValidator implements ConstraintValidator<Priority, String> {

    @Override
    public void initialize(Priority priority) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {

        if(CEPATSUtil.isEmpty(s) || Arrays.asList(CEPATSConstants.priorityValues).contains(s)) return true;
        return false;
    }
}
