package com.tarion.cepats.domains.crm;

import com.tarion.cepats.domains.crm.entities.AgreementLineEntity;
import com.tarion.cepats.domains.crm.entities.WSCAssignedEntity;
import com.tarion.cepats.domains.crm.entities.WSRAssignedEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


/**
 * @author oagady
 * Oct 1, 2014
 */
@Repository
public interface WSAssignedRepository extends JpaRepository<AgreementLineEntity, Long> {
	
    @Query("SELECT entity FROM WSCAssignedEntity entity WHERE entity.enrolmentNumber = ?1")
    public WSCAssignedEntity getWSCAssignedToCEPATS(String enrolmentNumber);
    
    @Query("SELECT entity FROM WSRAssignedEntity entity WHERE entity.enrolmentNumber = ?1")
    public WSRAssignedEntity getWSRAssignedToCEPATS(String enrolmentNumber);
     
}
