/**
 * 
 */
package com.tarion.cepats.web.dtos;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;

/**
 * @author <A href="mailto:joni.paananen@tarion.com">Joni Paananen</A>
 *
 * @since Nov 7, 2014
 */
public class FormItemAttachmentDto {

    private Long id;
    private String originalFileName;
    private String cmItemId;
    private Timestamp uploadedDateTime;
    private String uploadedUser;
    private UserTypeEnum uploadedUserType;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOriginalFileName() {
		return originalFileName;
	}
	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}
	public String getCmItemId() {
		return cmItemId;
	}
	public void setCmItemId(String cmItemId) {
		this.cmItemId = cmItemId;
	}
	public Timestamp getUploadedDateTime() {
		return uploadedDateTime;
	}
	public void setUploadedDateTime(Timestamp uploadedDateTime) {
		this.uploadedDateTime = uploadedDateTime;
	}
	public String getUploadedUser() {
		return uploadedUser;
	}
	public void setUploadedUser(String uploadedUser) {
		this.uploadedUser = uploadedUser;
	}
	public UserTypeEnum getUploadedUserType() {
		return uploadedUserType;
	}
	public void setUploadedUserType(UserTypeEnum uploadedUserType) {
		this.uploadedUserType = uploadedUserType;
	}
	
	public String getFormattedUploadedDateTime() {
		return new SimpleDateFormat(CEPATSConstants.ATTACHMENT_UPLOADED_DATE_TIME_FORMAT).format(uploadedDateTime);
	}
}
