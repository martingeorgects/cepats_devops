package com.tarion.cepats.domains.cepats.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.tarion.cepats.validators.CondoPosition;
import com.tarion.cepats.validators.Priority;
import com.tarion.cepats.validators.VendorPosition;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 05, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */

@Entity
@Table(name = "FORM_ITEMS")
public class FormItemEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String FIELD_EMPTY_MESSAGE = " is a mandatory field. Please complete this field or delete this item";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="itemId")
    private Integer itemId;
    
    @Column(name="updatedUserFirstName")
    private String updatedUserFirstName;
    
    @Column(name="updatedUserLastName")
    private String updatedUserLastName;

    @Column(name = "enrolmentNumber")
    private String enrolmentNumber;

    @Column(name = "formType")
    @Enumerated(EnumType.STRING)
    private FormTypeEnum formType;

    @Column(name = "refId")
    @NotNull(message = "The PA Ref. #" + FIELD_EMPTY_MESSAGE)
    @Size(max = 50, message = "The PA Ref. # cannot exceed 50 characters. Please correct")
    private String auditRefNum;

    @Column(name = "deficiencyDescription")
    @NotNull(message = "The Deficiency Description" + FIELD_EMPTY_MESSAGE)
    @Size(max = 2000, message = "The Deficiency Description cannot exceed 2000 characters. Please correct")
    private String deficiencyDescription;

    @Column(name = "deficiencyLocation")
    @NotNull(message = "The Deficiency Location" + FIELD_EMPTY_MESSAGE)
    @Size(max = 950, message = "The Deficiency Location cannot exceed 950 characters. Please correct")
    private String deficiencyLocation;

    @Column(name = "priority")
    @Priority
    private String priority;

    @Column(name = "vendorsPosition")
    @VendorPosition
    private String vendorsPosition;

    @Column(name = "vendorsResponse")
    @Size(max = 5000, message = "The Vendor's response cannot exceed 5000 characters. Please correct.")
    private String vendorsResponse;

    @Column(name = "condoCorpPosition")
    @CondoPosition
    private String condoCorpPosition;

    @Column(name = "condoCorpResponse")
    @Size(max = 5000, message = "The Condo Corp Response cannot exceed 5000 characters. Please correct.")
    private String condoCorpResponse;
    
    @Column(name = "fundingAndDescription")
    @Size(max = 200, message = "The PEF cannot exceed 200 characters. Please correct.")
    private String fundingAndDescription;

    @Column(name = "updateType")
    @Enumerated(EnumType.STRING)
    private UpdateTypeEnum updateType;

    @Column(name = "updatedUser")
    private String user;
   
    @Column(name = "updatedUserEmailAddress")
    private String updatedUserEmailAddress;

    @Column(name = "updatedUserType")
    @Enumerated(EnumType.STRING)
    private UserTypeEnum userType;

    @Column(name = "updatedDateTime")
    private Timestamp updatedDateTime;

    @Column(name = "version")
    private Integer version;

    @Column(name = "uploadedToCRM")
    private boolean uploadedToCRM;
    
    @Column(name = "itemAdded")
    private boolean itemAdded;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getEnrolmentNumber() {
        return enrolmentNumber;
    }

    public void setEnrolmentNumber(String enrolmentNumber) {
        this.enrolmentNumber = enrolmentNumber;
    }

    public FormTypeEnum getFormType() {
        return formType;
    }

    public void setFormType(FormTypeEnum formType) {
        this.formType = formType;
    }

    public String getAuditRefNum() {
        return auditRefNum;
    }

    public void setAuditRefNum(String refId) {
        this.auditRefNum = refId;
    }

    public String getDeficiencyDescription() {
        return deficiencyDescription;
    }

    public void setDeficiencyDescription(String deficiencyDescription) {
        this.deficiencyDescription = deficiencyDescription;
    }

    public String getDeficiencyLocation() {
        return deficiencyLocation;
    }

    public void setDeficiencyLocation(String deficiencyLocation) {
        this.deficiencyLocation = deficiencyLocation;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getVendorsPosition() {
        return vendorsPosition;
    }

    public void setVendorsPosition(String vendorsPosition) {
        this.vendorsPosition = vendorsPosition;
    }

    public String getVendorsResponse() {
        return vendorsResponse;
    }

    public void setVendorsResponse(String vendorsResponse) {
        this.vendorsResponse = vendorsResponse;
    }

    public String getCondoCorpPosition() {
        return condoCorpPosition;
    }

    public void setCondoCorpPosition(String condoCorpPosition) {
        this.condoCorpPosition = condoCorpPosition;
    }

    public String getCondoCorpResponse() {
        return condoCorpResponse;
    }

    public void setCondoCorpResponse(String condoCorpResponse) {
        this.condoCorpResponse = condoCorpResponse;
    }

    public UpdateTypeEnum getUpdateType() {
        return updateType;
    }

    public void setUpdateType(UpdateTypeEnum updateType) {
        this.updateType = updateType;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public UserTypeEnum getUserType() {
        return userType;
    }

    public void setUserType(UserTypeEnum userType) {
        this.userType = userType;
    }

    public Timestamp getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Timestamp updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion;
    }

    public boolean isUploadedToCRM() {
        return uploadedToCRM;
    }

    public void setUploadedToCRM(boolean uploadedToCRM) {
        this.uploadedToCRM = uploadedToCRM;
    }

	public String getUpdatedUserFirstName() {
		return updatedUserFirstName;
	}

	public void setUpdatedUserFirstName(String updatedUserFirstName) {
		this.updatedUserFirstName = updatedUserFirstName;
	}

	public String getUpdatedUserLastName() {
		return updatedUserLastName;
	}

	public void setUpdatedUserLastName(String updatedUserLastName) {
		this.updatedUserLastName = updatedUserLastName;
	}

	public String getUpdatedUserEmailAddress() {
		return updatedUserEmailAddress;
	}

	public void setUpdatedUserEmailAddress(String updatedUserEmailAddress) {
		this.updatedUserEmailAddress = updatedUserEmailAddress;
	}

	public boolean isItemAdded() {
		return itemAdded;
	}

	public void setItemAdded(boolean itemAdded) {
		this.itemAdded = itemAdded;
	}

	public String getFundingAndDescription() {
		return fundingAndDescription;
	}

	public void setFundingAndDescription(String fundingAndDescription) {
		this.fundingAndDescription = fundingAndDescription;
	}

}
