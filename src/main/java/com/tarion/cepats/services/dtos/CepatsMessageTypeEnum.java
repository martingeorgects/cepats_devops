/* 
 * 
 * RegistrationsMessageType.java
 *
 * Copyright (c) 2014 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.cepats.services.dtos;

/**
 * Used for determining which cepats message should be posted to Cepats Queue
 * 
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2014-01-29
 * @version 1.0
 *
 */
public enum CepatsMessageTypeEnum {
	CEPATS_MESSAGE_IN_LINE_ITEMS_RESPONSE("Cepats Message InLineItems Response");
	
	private String label;
	
	private CepatsMessageTypeEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
}