package com.tarion.cepats.domains.cepats;

import com.tarion.cepats.domains.cepats.entities.ConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author <a href="bojan.volcansek@tarion.com">msalman</a>
 * @since Oct 11, 2014
 * Copyright (c) 2014 Tarion Warranty Corporation.
 * All rights reserved.
 */
@Repository
public interface ConfigRepository extends JpaRepository<ConfigEntity, String> {

    public List<ConfigEntity> findAll();
}
