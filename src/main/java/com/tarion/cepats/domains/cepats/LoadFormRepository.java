package com.tarion.cepats.domains.cepats;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class LoadFormRepository {
	
	//Optimized query to improve performance for loading large forms.
	//This query returns both item data, and attachment count for an item
    public static final String LOAD_FORM  = "  SELECT item.id, item.itemId, item.refId, item.enrolmentNumber, item.formType,  " 
    		+ "item.deficiencyDescription, item.deficiencyLocation, item.priority, item.vendorsPosition, item.vendorsResponse, item.condoCorpPosition, " 
    		+ " item.condoCorpResponse, item.fundingAndDescription, att.attCount, item.itemAdded, item.updatedDateTime, item.updatedUserFirstName, item.updatedUserLastName, "
    		+ " item.updatedUserType "
    		+ "FROM FORM_ITEMS item "
    		+ "LEFT JOIN (SELECT formItemId, COUNT(formItemId) as attCount FROM FORM_ITEMS_ATTACHMENTS GROUP BY formItemId) att "
    		+ "on att.formItemId = item.id "
    		+ "WHERE item.formType = ?2 AND item.enrolmentNumber = ?1 ORDER BY item.itemId ASC";

    private EntityManager em;

    public LoadFormRepository(EntityManagerFactory cepatsEntityManagerFactory){
        this.em = cepatsEntityManagerFactory.createEntityManager();
    }

    @Transactional
    public List<Object[]> loadForm(String enrolmentNumber, String formType) {
        Query query = em.createNativeQuery(LOAD_FORM);
        query.setParameter(1, enrolmentNumber);
        query.setParameter(2, formType);
        List<Object[]> result = (List<Object[]>)query.getResultList();
        return result;
    }

}
