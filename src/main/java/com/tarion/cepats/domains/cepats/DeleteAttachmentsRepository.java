package com.tarion.cepats.domains.cepats;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;


@Repository
public class DeleteAttachmentsRepository {

    public static final String DELETE_ATTACHMENTS  = "delete from  FORM_ITEMS_ATTACHMENTS where formItemId in " + 
    		"(select id from FORM_ITEMS where enrolmentNumber=?1 and formType=?2)";

    private EntityManager em;

    public DeleteAttachmentsRepository(EntityManagerFactory cepatsEntityManagerFactory){
        this.em = cepatsEntityManagerFactory.createEntityManager();
    }

    @Transactional
    public void deleteAttachmentsByEnrolmentNumberAndFormType(String enrolmentNumber, String formType) {
        Query query = em.createNativeQuery(DELETE_ATTACHMENTS);
        query.setParameter(1, enrolmentNumber);
        query.setParameter(2, formType);
        query.executeUpdate();
    }

}
