package com.tarion.cepats.services.attachments;

import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;

import com.tarion.tbi.cm.attachments.Attachment;
import com.tarion.tbi.cm.attachments.Attachments;
import com.tarion.tbi.cm.attachments.CmAttribute;
import com.tarion.tbi.cm.attachments.CmAttributes;


/**
 * Utility class, based on Test Data Builder pattern, used for creating
 * Attachments objects that are passed as arguments to Attachments Web Service.
 * 
 * TODO - For review - I am using original filename for each individual
 * attachment as tracking number, so that I can store it in BSA XREF table.
 * Perhaps the TBI Web Service should be changed to include original filename as
 * input and output parameters.
 * 
 * @author abasaric
 * 
 */
public class AttachmentBuilder {

	private static final String VB_NUMBER = "VbNumber";
	private static final String CM_FILE_NAME_ATTR = "CMFileName";
	public static final String ENROLMENT_NUMBER = "EnrolmentNumber";
	public static final String RECEIVED_DATE = "ReceivedDate";
	public static final String ITEM_TYPE_HOME_DOCUMENTS = "HomeDocuments";
	public static final String ITEM_ATTR_DOC_NAME = "DocName";
	public static final String CANCELLATION_DOCUMENT = "Cancellation Document";

	// Used for cancellation attachments
	private String enrolmentNumber;

	private String vbNumber;
	
	String trackingNum;
	List<AttachmentDto> attachmentDtos;
	CmAttributes cmAttributes = new CmAttributes();


	/**
	 * This constructor is used only for Cancellation Form attachments which are
	 * inserted into CM as HomeDocuments
	 */
	public static AttachmentBuilder makeCancellationAttachment(String enrolmentNumber, String trackingNum, List<AttachmentDto> attachmentDtos) {
		for (AttachmentDto dto : attachmentDtos) {
			dto.setItemType(ITEM_TYPE_HOME_DOCUMENTS);			
		}
		
		return new AttachmentBuilder(enrolmentNumber, trackingNum, attachmentDtos);
		
	}

	
	
	
	public AttachmentBuilder(String trackingNum, List<AttachmentDto> attachmentDtos) {
		super();
		this.trackingNum = trackingNum;
		this.attachmentDtos = attachmentDtos;
	}

	private AttachmentBuilder(String trackingNum, List<AttachmentDto> attachmentDtos,
			CmAttributes cmAttributes) {
		this(trackingNum, attachmentDtos);
		this.cmAttributes = cmAttributes;
	}

	
	// TODO Clean up handling of item attributes - perhaps add field for attachmentDtos and add them to target object in buildAndWrap method
	public AttachmentBuilder(String enrolmentNumber, String trackingNum, List<AttachmentDto> attachmentDtos) {
		this(trackingNum, attachmentDtos);
		this.enrolmentNumber = enrolmentNumber;
		Map<String,String> attrs = attachmentDtos.get(0).getItemAttributes();
		if(attrs.containsKey(VB_NUMBER)){
			vbNumber = attrs.get(VB_NUMBER);
		}
	}


	/**
	 * This method is used only for cancellation form attachments.
	 * 
	 * Builds list of Attachment objects (@Xml anotated class which is passed
	 * on to TBI web service), from the list of DTOs (attachmentDtos), and wraps
	 * it into the Attachment object.
	 */
	public Attachments buildAndWrapCancellationAttachment() {
		Attachments ret = new Attachments();
		for (AttachmentDto dto : attachmentDtos) {
			Attachment att = new Attachment();
			att.setBytes(dto.getDocData());
			att.setFileType(dto.getMimeFileType());

			String origFileName = upTo25LastChars(dto.getFilename());
			att.setClientTrackingId(origFileName);

			att.setCmItemType(ITEM_TYPE_HOME_DOCUMENTS);
			att.setCmAttributes(cmAttributes);
			ret.getAttachment().add(att);

			CmAttribute cmFileName = new CmAttribute();
			cmFileName.setName(CM_FILE_NAME_ATTR);
			cmFileName.setValue(upTo25LastChars(dto.getFilename()));
			cmAttributes.getCmAttribute().add(cmFileName);

			CmAttribute cmDocName = new CmAttribute();
			cmDocName.setName(ITEM_ATTR_DOC_NAME);
			cmDocName.setValue(CANCELLATION_DOCUMENT);
			cmAttributes.getCmAttribute().add(cmDocName);
			
			CmAttribute enrNum = new CmAttribute();
			enrNum.setName(ENROLMENT_NUMBER);
			enrNum.setValue(enrolmentNumber);
			cmAttributes.getCmAttribute().add(enrNum);
			
			CmAttribute vbNum = new CmAttribute();
			vbNum.setName(VB_NUMBER);
			vbNum.setValue(vbNumber);
			cmAttributes.getCmAttribute().add(vbNum);
			
		}
		return ret;
	}

	/**
	 * This method is used only generic upload attachment form.
	 * 
	 * Builds list of Attachment objects (@Xml anootated class which is passed
	 * on to TBI web service), from the list of DTOs (attachmentDtos), and wraps
	 * it into the Attachment object.
	 */
	public Attachments buildAndWrapGenericAttachment() {
		Attachments ret = new Attachments();
		for (AttachmentDto dto : attachmentDtos) {
			Attachment att = initializeAttachmentAndPoulateCommonAttibutes(dto.getItemType(), dto.getDocData(), dto.getMimeFileType());
			populateAttributesPerFile(att, dto.getItemAttributes());
			ret.getAttachment().add(att);
		}
		return ret;
	}

	/**
	 * Creates an Attachment object and sets most important fields, that are common for all uploaded files
	 */
	private Attachment initializeAttachmentAndPoulateCommonAttibutes(String itemType, byte[] docData, String mimeType) {
		Attachment att = new Attachment();
		att.setCmAttributes(new CmAttributes());
		att.setCmItemType(itemType);
		att.setBytes(docData);
		att.setFileType(mimeType);
		populateCommonAttributes(att, cmAttributes);
		// TODO what is the best use for client tracking id
		// att.setClientTrackingId(???);
		return att;
	}

	/**
	 * Populates attributes that are potentially different for each file being uploaded
	 */
	private void populateAttributesPerFile(Attachment att, Map<String, String> attrDtos) {
		// TODO Where do I validate type and size of attribute?
		for (String name : attrDtos.keySet()) {
			CmAttribute attr = new CmAttribute();
			attr.setName(name);
			attr.setValue(attrDtos.get(name));
			att.getCmAttributes().getCmAttribute().add(attr);
		}
	}

	/**
	 * Populates attributes that are common for all uploaded files
	 */
	public void populateCommonAttributes(Attachment att, CmAttributes cmAttributes) {
		att.getCmAttributes().getCmAttribute().addAll(cmAttributes.getCmAttribute());
	}

	/**
	 * Returns 25 last characters of input string, or whole string if it is
	 * shorter than that.
	 */
	public static String upTo25LastChars(String in) {
		String ret = "";
		if (in != null) {
			int start = in.length() > 25 ? in.length() - 25 : 0;
			ret = in.substring(start);
		}
		return ret;
	}

	public AttachmentBuilder withEnrolmentNumber(String value) {
		return this.withAttribute(ENROLMENT_NUMBER, value);
	}

	public AttachmentBuilder withAttribute(String name, String value) {
		CmAttribute attr = new CmAttribute();
		attr.setName(name);
		attr.setValue(value);
		cmAttributes.getCmAttribute().add(attr);
		AttachmentBuilder ret = new AttachmentBuilder(trackingNum, attachmentDtos, cmAttributes);
		return ret;
	}

	public AttachmentBuilder receivedToday() {
		return this.withAttribute(RECEIVED_DATE, todayFormattedForCm());
	}

	private String todayFormattedForCm() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(new java.util.Date());
	}

	// TODO Throw more specific exception?
	public static String toXmlString(Attachments attachments) {

		try {
			JAXBContext ctx = JAXBContext.newInstance(attachments.getClass());
			Marshaller m = ctx.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();

			JAXBElement<Attachments> jaxbElement = new JAXBElement<Attachments>(new QName(null, "attachments"),
					Attachments.class, attachments);
			m.marshal(jaxbElement, sw);

			return sw.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	// TODO Throw more specific exception?
	public static Attachments fromXmlString(String xml) {
		try {
			JAXBContext ctx = JAXBContext.newInstance(Attachments.class);
			Unmarshaller unmarshaller = ctx.createUnmarshaller();

			final StringReader xmlReader = new StringReader(xml);
			final StreamSource xmlSource = new StreamSource(xmlReader);
			JAXBElement<Attachments> fromDb = unmarshaller.unmarshal(xmlSource, Attachments.class);
			return fromDb.getValue();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
