package com.tarion.cepats.domains.cepats;

import com.tarion.cepats.domains.cepats.entities.FormItemHistoryEntity;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 18, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
@Repository
public interface FormItemHistoryRepository extends JpaRepository<FormItemHistoryEntity, Long> {

    @Query("SELECT max(item.version) FROM FormItemHistoryEntity item WHERE item.itemId = ?1 AND item.formType = ?2 AND item.enrolmentNumber = ?3")
    public Integer getLastVersionNumber(int itemId, FormTypeEnum formType, String enrolmentNumber);
    
    @Query(value="SELECT item FROM FormItemHistoryEntity item WHERE item.updatedDateTime > ?2 AND item.enrolmentNumber = ?1  ORDER BY item.enrolmentNumber, item.itemId, item.version DESC")
    public List<FormItemHistoryEntity> findByEnrolmentNumberAndDate(String enrolmentNumber, Timestamp timestamp);
    
    @Query(value="SELECT item.enrolmentNumber FROM  FormItemHistoryEntity item  WHERE item.updatedDateTime > ?1 AND item.formType = ?2 GROUP BY item.enrolmentNumber")
    public List<String> findEnrolmentNumbersByDateAndFormType(Timestamp timestamp, FormTypeEnum formType);

    @Query(value="SELECT item.enrolmentNumber FROM  FormItemHistoryEntity item  WHERE item.updatedDateTime > ?3 AND item.enrolmentNumber = ?1 AND item.itemId = ?2")
    public List<String> findByEnrolmentNumberAndItemIdAndDate(String enrolmentNumber, int itemId, Timestamp timestamp);
    
    @Query(value="SELECT item FROM  FormItemHistoryEntity item  WHERE item.enrolmentNumber = ?1 AND item.itemId = ?2 AND item.formType = ?3 AND item.updatedDateTime between ?4 AND ?5 ORDER BY item.version DESC")
    public List<FormItemHistoryEntity> findByEnrolmentNumberAndItemIdAndFormTypeAndDateRange(String enrolmentNumber, int itemId, FormTypeEnum formType,  Timestamp startTime, Timestamp endTime);
    
    @Query(value="SELECT item FROM  FormItemHistoryEntity item  WHERE item.enrolmentNumber = ?1 AND item.itemId = ?2 AND item.formType = ?3 ORDER BY item.version DESC")
    public List<FormItemHistoryEntity> findByEnrolmentNumberAndItemIdAndFormType(String enrolmentNumber, int itemId, FormTypeEnum formType);
   
    @Query(value="SELECT item FROM  FormItemHistoryEntity item  where item.enrolmentNumber = ?1 AND item.formType = ?2 ORDER BY item.itemId DESC, item.version DESC")
    public List<FormItemHistoryEntity> findByEnrolmentNumberAndFormType(String enrolmentNumber, FormTypeEnum formType);
    
    @Query(value="SELECT updatedUserEmailAddress FROM  FORM_ITEMS_HISTORY  WHERE enrolmentNumber = ?1 AND formType = ?2 AND updatedUserType = ?3 AND enrolmentNumber IS NOT NULL GROUP BY updatedUserEmailAddress", nativeQuery = true)
    public List<String> findEmailAddressesforEnrolmentAndFormTypeAndUserType(String enrolmentNumber, String formType, String userType);
   
    @Query(value="SELECT item FROM  FormItemHistoryEntity item  WHERE item.updatedDateTime between ?4 AND ?5 AND item.enrolmentNumber = ?1 AND item.itemId = ?2 AND item.formType = ?3")
    public List<FormItemHistoryEntity> findByEnrolmentNumberAndFormTypeAndItemIdAndDateRange(String enrolmentNumber, int itemId, String formType, Timestamp startDate, Timestamp endDate);
    
    public List<FormItemHistoryEntity> findByUpdatedUserEmailAddressAndFormTypeAndUserType(String emailAddress, FormTypeEnum formType, UserTypeEnum userType, Pageable pageable); 
 
    @Modifying
    @Transactional
    @Query("DELETE FROM FormItemHistoryEntity item WHERE item.enrolmentNumber = ?1 AND item.formType = ?2")
    public void deleteByEnrolmentNumberAndFormType(String enrolmentNumber, FormTypeEnum formType);
}
