/* 
 * 
 * AttachmentDto.java
 *
 * Copyright (c) 2013 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.cepats.services.attachments;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class AttachmentDto.
 * 
 * TODO - Copied from TBI project, modified.
 * 
 * @author Alex Basarc
 * @since 14-Jun-2013
 * @version 1.0
 */
public class AttachmentDto implements Serializable {
    private static final long serialVersionUID = 7124042072776514153L;

    private String itemType;
    private Map<String, String> itemAttributes = new HashMap<String, String>();
    
    private String mimeFileType;
    private String filename;
    private byte[] docData;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMimeFileType() {
        return mimeFileType;
    }

    public void setMimeFileType(String mimeFileType) {
        this.mimeFileType = mimeFileType;
    }

    public byte[] getDocData() {
        return docData;
    }

    public void setDocData(byte[] docData) {
        this.docData = docData;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public Map<String, String> getItemAttributes() {
        return itemAttributes;
    }

    public void setItemAttributes(Map<String, String> itemAttributes) {
        this.itemAttributes = itemAttributes;
    }
 
    
    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append("AttachmentDto [");
        ret.append("itemType: ").append(itemType);
        ret.append("itemAttributes: ").append(itemAttributes);
        ret.append("mimeFileType: ").append(mimeFileType);
        ret.append("filename: ").append(filename);
        return ret.toString();
    }
    
}