package com.tarion.cepats.domains.cepats.entities;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 05, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
public enum FormTypeEnum {
    FIRSTYEAR("1YR"),
    SECONDYEAR("2YR");

    private String label;

    FormTypeEnum(String label) { this.label = label; }
    public String getLabel() { return label; }
}
