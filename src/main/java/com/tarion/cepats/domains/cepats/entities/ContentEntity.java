/* 
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.cepats.domains.cepats.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

 /**
 * 
 * @author oagady
 * @date Nov 17, 2014
 * @version 1.0
 *
 */
@Entity
@Table(name = "CEPATS_CONTENT")
public class ContentEntity implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    private Integer id;
    private Integer contentNumber;
    private String contentType;
    private String contentText1;
    private String contentText2;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getContentNumber() {
		return contentNumber;
	}
	public void setContentNumber(Integer contentNumber) {
		this.contentNumber = contentNumber;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getContentText1() {
		return contentText1;
	}
	public void setContentText1(String contentText1) {
		this.contentText1 = contentText1;
	}
	public String getContentText2() {
		return contentText2;
	}
	public void setContentText2(String contentText2) {
		this.contentText2 = contentText2;
	}


}
