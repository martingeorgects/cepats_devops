/* $Id$
 *
 * Copyright (c) 2014 Tarion Warranty Corporation.
 * All rights reserved. 
 */
package com.tarion.cepats.services;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.services.dtos.CepatsMessageTypeEnum;
import com.tarion.cepats.services.exception.CepatsServiceException;
import com.tarion.cepats.common.LoggerUtil;

/**
 * This class is used to post a message to a JMS queue
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since October 03, 2014
 * @version 1.0
 */
@Service
public class JmsMessageSender {
	
	@Autowired
	PropertyService propertyService;
	
	/**
	 * Post the message to the Cepats Submit Request Queue
	 * 
	 * @param docImport
	 * @param pid
	 * @throws TarionIntegratorException
	 */
	public void postToCepatsSubmitRequestQueue(String xmlMessageText, CepatsMessageTypeEnum messageType) throws CepatsServiceException {
		long startTime = LoggerUtil.logEnter(JmsMessageSender.class, "postToCepatsSubmitRequestQueue", xmlMessageText, messageType);
		sendTxtMessageToQueue(CEPATSConstants.CEPATS_CRM_RESPONSE_QUEUE_NAME, xmlMessageText, null, true, CEPATSConstants.CEPATS_CRM_CONNECTION_FACTORY_NAME, messageType);
		LoggerUtil.logExit(JmsMessageSender.class, "postToCepatsSubmitRequestQueue", startTime);
	}

	/**
	 * Send text message to CRM
	 * 
	 * @param queueJndiName
	 *            The JNDI name of the queue
	 * @param msg
	 *            The message to send
	 * @param trackingNumber
	 *            Tracking number
	 * @throws TarionIntegratorException
	 *             Problem sending the message
	 */
	private void sendTxtMessageToQueue(String queueJndiName, String msg, String trackingNumber, boolean setHeader, String connectionFactoryName, CepatsMessageTypeEnum cepatsMessageType) throws CepatsServiceException {

		Connection connection = null;
		Session session = null;
		MessageProducer producer = null;
		TextMessage textMessage = null;

		try {
			InitialContext ic = new InitialContext();
			ConnectionFactory connectionFactory = (ConnectionFactory) ic.lookup(connectionFactoryName);

			Queue theQueue = (Queue) ic.lookup(queueJndiName);

			connection = connectionFactory.createConnection();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			producer = session.createProducer(theQueue);
			textMessage = session.createTextMessage();
			if (setHeader) {
				setHeader(textMessage, queueJndiName, cepatsMessageType);
			}

			textMessage.setText(msg);
			// use the tracking number as the correlation id
			textMessage.setJMSCorrelationID(trackingNumber);

			producer.send(textMessage);

		} catch (NamingException e) {
			LoggerUtil.logError(JmsMessageSender.class, e.getLocalizedMessage(), e);
			throw new CepatsServiceException(e.getMessage());
		} catch (JMSException je) {
			String errMsg = "sendTxtMessageToQueue for queue=[" + queueJndiName + "] failed: " + je.toString();
			throw new CepatsServiceException(errMsg, je);
		} finally {
			close(producer);
			close(session);
			close(connection);
			connection = null;
			session = null;
			producer = null;
		}
	}

	/**
	 * Close the connection
	 * 
	 * @param connection
	 *            Connection to close
	 */
	private void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (JMSException e) {
				LoggerUtil.logError(JmsMessageSender.class, e.getLocalizedMessage(), e);
				throw new CepatsServiceException(e.getMessage());
			}
		}
	}

	/**
	 * Close the message producer
	 * 
	 * @param producer
	 *            Message producer to close
	 */
	private void close(MessageProducer producer) {
		if (producer != null) {
			try {
				producer.close();
			} catch (JMSException e) {
				LoggerUtil.logError(JmsMessageSender.class, e.getLocalizedMessage(), e);
				throw new CepatsServiceException(e.getMessage());
			}
		}
	}

	/**
	 * Close the session
	 * 
	 * @param session
	 *            Session to close
	 */
	private void close(Session session) {
		if (session != null) {
			try {
				session.close();
			} catch (JMSException e) {
				LoggerUtil.logError(JmsMessageSender.class, e.getLocalizedMessage(), e);
				throw new CepatsServiceException(e.getMessage());
			}
		}
	}

	/**
	 * Set the CRM Header information for create case
	 * 
	 * @param txtMsg
	 *            Current TextMessage
	 * @param queueJndiName
	 *            JNDI name of the queue
	 * @throws JMSException
	 *             Problem setting the property
	 * @throws TarionIntegratorException
	 *             Problem with the properties file
	 */
	private void setHeader(TextMessage txtMsg, String queueJndiName, CepatsMessageTypeEnum cepatsMessageType) throws JMSException, CepatsServiceException {

		if (CEPATSConstants.CEPATS_CRM_RESPONSE_QUEUE_NAME.equals(queueJndiName)) {
			if (cepatsMessageType != null) {
				txtMsg.setStringProperty("RequestingNode", propertyService.getCrmResponseQueueRequestingNode());
				txtMsg.setStringProperty("FinalDestinationNode", propertyService.getJmsDestinationNode());
				txtMsg.setStringProperty("DestinationNode", propertyService.getJmsDestinationNode());
				txtMsg.setStringProperty("JMSProvider", propertyService.getCrmResponseQueueJMSProvider());
				txtMsg.setStringProperty("MessageType", propertyService.getCrmResponseQueueJMSMessageType());
				txtMsg.setStringProperty("version", propertyService.getCrmResponseQueueVersion());
				txtMsg.setStringProperty("JMSMessageType", propertyService.getCrmResponseQueueJMSMessageType());
				txtMsg.setStringProperty("Password", propertyService.getCrmResponseQueuePassword());
				if (cepatsMessageType == CepatsMessageTypeEnum.CEPATS_MESSAGE_IN_LINE_ITEMS_RESPONSE) {
					txtMsg.setStringProperty("MessageName", propertyService.getCrmResponseQueueMessageName());
				}
			} 
		}		
	}

}