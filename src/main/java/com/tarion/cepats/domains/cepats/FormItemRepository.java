package com.tarion.cepats.domains.cepats;

import com.tarion.cepats.domains.cepats.entities.FormItemEntity;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.List;


/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 17, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
@Repository
public interface FormItemRepository extends JpaRepository<FormItemEntity, Long> {

    @Query("SELECT max(item.itemId) FROM FormItemEntity item WHERE item.formType = ?1 AND item.enrolmentNumber = ?2")
    public Integer getLastItemId(FormTypeEnum formType, String enrolmentNumber);

    @Query("SELECT item FROM FormItemEntity item WHERE item.formType = ?3 AND item.enrolmentNumber = ?2 AND item.itemId = ?1")
    public FormItemEntity getExistingItem(Integer itemId, String enrolmentNumber, FormTypeEnum formType);

    public Long countByEnrolmentNumberAndFormType(String enrolmentNumber, FormTypeEnum formType);

    @Query("SELECT item FROM FormItemEntity item WHERE item.formType = ?2 AND item.enrolmentNumber = ?1 ORDER BY item.itemId ASC")
    public List<FormItemEntity> findAll(String enrolmentNumber, FormTypeEnum formType);

    @Query("SELECT item FROM FormItemEntity item WHERE item.enrolmentNumber = ?1 ORDER BY item.formType ASC, item.itemId ASC")
    public List<FormItemEntity> findAll(String enrolmentNumber);

    @Query("SELECT count(item.id) FROM FormItemEntity item WHERE  item.enrolmentNumber = ?1")
    public int findCEPATSForEnrolmentNumber(String enrolmentNumber);
    
    @Query(value="SELECT updatedUserEmailAddress FROM  FORM_ITEMS  WHERE enrolmentNumber = ?1 AND formType = ?2 AND enrolmentNumber IS NOT NULL GROUP BY updatedUserEmailAddress", nativeQuery = true)
    public List<String> findEmailAddressesforEnrolmentAndFormType(String enrolmentNumber, String formType);
    
    public FormItemEntity findByEnrolmentNumberAndItemIdAndFormType(String enrolmentNumber, int itemId, FormTypeEnum formType);

    @Query(value="SELECT item FROM  FormItemEntity item  where item.enrolmentNumber = ?1 AND item.formType = ?2 AND item.updatedDateTime between ?3 AND ?4 ORDER BY item.itemId ASC, item.version DESC")
	public List<FormItemEntity> findByEnrolmentNumberAndFormTypeAndTimestamp(String enrolmentNumber, FormTypeEnum formType, Timestamp startTime, Timestamp endTime);
    
    @Query(value="SELECT item FROM  FormItemEntity item  where item.enrolmentNumber = ?1 AND item.formType = ?2 ORDER BY item.itemId ASC, item.version DESC")
 	public List<FormItemEntity> findByEnrolmentNumberAndFormType(String enrolmentNumber, FormTypeEnum formType);
    
    @Query(value="SELECT item FROM FormItemEntity item WHERE item.id = ?1")
    public FormItemEntity getFormItem(long id); 
    
    @Query(value="SELECT item.enrolmentNumber FROM  FormItemEntity item  WHERE item.updatedDateTime > ?1 AND item.formType = ?2 GROUP BY item.enrolmentNumber")
    public List<String> findEnrolmentNumbersByDateAndFormType(Timestamp timestamp, FormTypeEnum formType);
    
    @Query(value="SELECT item.id FROM  FormItemEntity item  WHERE item.enrolmentNumber = ?1 AND item.formType = ?2")
    public List<Long> findIDByEnrolmentNumberAndFormType(String enrolmentNumber, FormTypeEnum formType);
    
    @Modifying
    @Transactional
    @Query("DELETE FROM FormItemEntity item WHERE item.enrolmentNumber = ?1 AND item.formType = ?2")
    public void deleteByEnrolmentNumberAndFormType(String enrolmentNumber, FormTypeEnum formType);
    
//    @Modifying
//    @Transactional
//    @Query("DELETE FROM FormItemHistoryEntity itemHistory WHERE itemHistory.enrolmentNumber = ?1 and itemHistory.itemId = ?2 AND itemHistory.formType = ?3")
//    public void deleteLineItemHistory(String enrolmentNumber, long itemId, FormTypeEnum formType);
    
    @Modifying
    @Transactional
    @Query("DELETE FROM FormItemEntity item WHERE item.enrolmentNumber = ?1 and item.itemId = ?2 AND item.formType = ?3")
    public void deleteLineItem(String enrolmentNumber, int itemId, FormTypeEnum formType);

    //Added the below call to get the email addresses of VB users from the form item table. 
    @Query(value="SELECT updatedUserEmailAddress FROM  FORM_ITEMS WHERE enrolmentNumber = ?1 AND formType = ?2 AND updatedUserType = ?3 AND enrolmentNumber IS NOT NULL GROUP BY updatedUserEmailAddress", nativeQuery = true)
    public List<String> findEmailAddressesforEnrolmentAndFormTypeAndUserType(String enrolmentNumber, String formType, String userType);
  
    //Added the below call to get the first and last name of the VB users to send in the email notification template.
    public List<FormItemEntity> findByUpdatedUserEmailAddressAndFormTypeAndUserType(String emailAddress, FormTypeEnum formType, UserTypeEnum userType, Pageable pageable);
}
