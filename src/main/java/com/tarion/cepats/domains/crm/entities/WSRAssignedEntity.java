package com.tarion.cepats.domains.crm.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author oagady
 * @date Oct 27, 2014
 * @version 1.0
 *
 */
@Entity
@Table(name = "PS_TWC_CE_WSR_VW")
public class WSRAssignedEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SITE_ID")
	private String enrolmentNumber;
	
	@Column(name = "BO_NAME_DISPLAY")
	private String wsrAssigned;
	
	
	@Column(name = "EMAIL_ADDR")
	private String emailAddress;
	
	
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	
	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

	public String getWsrAssigned() {
		return wsrAssigned;
	}

	public void setWsrAssigned(String wsrAssigned) {
		this.wsrAssigned = wsrAssigned;
	}



}
