function attachmentsModal(formItemId) {

    var template = cepats.getTemplateAjax("attachments.modal.template");
    
    var attList = getAttachmnents(formItemId);
    
    var data = {
		id: formItemId,
		attachments: attList
    };
    

    $("#attachmentsModal")
        .html(template(data))
        .modal("show")
        .on("shown.bs.modal", function(){
        	if(cepats.readOnly){
        		$(".addAttachmentUploadForm").remove();
        		$("#uploadAttachment").remove();
        	}
            uploadFile();
        });
}


function uploadFile() {

    $("#viewAttachmentsForm")
        .bootstrapValidator({
            live: "enabled",            
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
            	attachment: {
                    validators: {
                    	notEmpty: {
                            message: "File is required"
                        },
                        file: {
                            maxSize: 25*1024*1024, //25 mb
                            extension: "jpeg,jpg,pdf,doc,docx",
                            type: "image/jpeg,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            message: "You can upload only in one of the following formats: doc, docx, pdf, jpg and jpeg (File size cannot exceed 25MB)."
                        }
                    }
                }
            }
        })
        .off('success.form.bv')		//Remove any previous event handlers attached to the success event
        .on('success.form.bv', function(e){

            e.preventDefault();
            console.log("view attachments form is valid sending request to server");
            var $form = $(e.target);
            $("#attachmentsModal").modal("hide");
            cepats.showWaitModal();

            var formData = new FormData();
            formData.append("file", $("[name='attachment']").prop("files")[0]);
            var url = "uploadAttachment/" +  $("input[name=attachmentsFormId]:hidden").val();

            cepats.saveForm(url, formData, "Sorry, we were unable to upload the attachment. Please try again.");
        });
    return false;
}

function getAttachmnents(formItemId) {
	
	var result;
	var success = true;
	
	var basePath = $.sessionStorage.get("basePath");
    $.ajax({
        type: "GET",
        async: false,
        url: basePath + "getAttachments/" + formItemId,
        processData: false,
        contentType: false,
        success: function(response){
            console.log(JSON.stringify(response));
            if (response != null) {
                result = response;
            }
        },
        error: function(errorThrown) {
            success = false;
        },
        failure: function(response){
            success = false;
        }
    });
    
    if(!success)
    	return null;
    
    return result;
}