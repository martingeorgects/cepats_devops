package com.tarion.cepats.web.dtos;

import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * @author <a href="muhammad.salman@tarion.com">msalman</a>
 * @since Sep 25, 2014
 * Copyright (c) 2009 Tarion Warranty Corporation.
 * All rights reserved.
 */
public class UpdateResultDto {

    private String message;
    private boolean error;
    private FormTypeEnum formType;
    private boolean showErrorDetails;
    private List<InvalidItem> invalidItems;
    private boolean initialUpload;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("error")
    public boolean hasError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public FormTypeEnum getFormType() {
        return formType;
    }

    public void setFormType(FormTypeEnum formType) {
        this.formType = formType;
    }

    public boolean isShowErrorDetails() {
        return showErrorDetails;
    }

    public void setShowErrorDetails(boolean showErrorDetails) {
        this.showErrorDetails = showErrorDetails;
    }

    public List<InvalidItem> getInvalidItems() {
        return invalidItems;
    }

    public void setInvalidItems(List<InvalidItem> invalidItems) {
        this.invalidItems = invalidItems;
    }
       

    public boolean isInitialUpload() {
		return initialUpload;
	}

	public void setInitialUpload(boolean initialUpload) {
		this.initialUpload = initialUpload;
	}



	public static class InvalidItem {

        private String errorMessage;
        private int rowNumber;

        public InvalidItem() {}

        public InvalidItem(int rowNumber, String errorMessage) {
            this.errorMessage = errorMessage;
            this.rowNumber = rowNumber;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public int getRowNumber() {
            return rowNumber;
        }

        public void setRowNumber(int rowNumber) {
            this.rowNumber = rowNumber;
        }
    }
}
