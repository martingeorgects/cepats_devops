cepats = {
  formType: null,
  formData: null,
  filteredData: null,
  searchString: null,
  searchItemId:null,
  searchRefNum: null,
  filterOption: null,
  basePath: null,
  openItem: null,
  itemsPerPage: 50,
  enrolmentNumber : null,
  userType : null,
  templates: [],
  deficiencyKeywords: [],
  priority: [],
  vendorsPosition: [],
  condoCorpPosition: [],
  currentPage: 1,
  isFiltered : false,
  isCondoConversionForm : false,
  readOnly : false,

	init : function(formType, enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
		this.formType = formType;
		this.basePath = window.location.protocol + "//" + window.location.host + "/cepats/";

    // Add active class to form type button
		var url =  location.pathname;
    $("nav a[href='" + url + "']").addClass("active");

		// Listeners
    $(document).on("click", "#exportPATSForm", function() {
    	cepats.exportForm();
    });

    $(document).on("click", '.deleteFormItem', function() {
      // Pass form item (table row) to the delete function
      cepats.showConfirmDeleteItemModal($(this).closest('.formItem'));
    });

		$(document).on("click", "#deletePATSBtn", function(){
			cepats.showConfirmDeletePATSModal();
		});

    $('.input-group.date').datepicker({
      format: 'yyyy-mm-dd'
    });

    $('select > option:first').hide();

    // Download History
    $('#download-history').on('click', function(){
      cepats.downloadHistory();
    });


    // Check to see if the window is top if not then display button
    $( document ).scroll(function(){
      if ($(this).scrollTop() > 300) {
        $('.scrollToTop').fadeIn();
      } else {
        $('.scrollToTop').fadeOut();
      }
    });
    
    // Click event to scroll to top
    $('.scrollToTop').click(function(){
      $('html,body').animate({scrollTop : 0},800);
      return false;
    });

     $.ajax({
        url: 'http://blog.tarion.com/feed/',
        type: 'GET',
        dataType: "xml"
    }).done(function(xml) {

        $.each($("item", xml), function(i, e) {

            var blogNumber = i + 1 + ". ";

            var itemURL = ($(e).find("link"));
            var blogURL = "<a href='" + itemURL.text() + "'>" + itemURL.text() +"</a>";

            var itemTitle = ($(e).find("title"));
            var blogTitle = "<h4>" + blogNumber + itemTitle.text() + "</h4>";

            $("#feed").append(blogTitle);
            $("#feed").append(blogURL);

        });
    });

		// $("#exportPATSTemplate").on("click", function(){
		// 	cepats.exportTemplate();
		// });

    filters.initFilters();
    this.initSearchInputs();
    this.setupPatsUploadForm();
    this.getUserInformation();
    this.getEnrolmentInfo();
    this.getReadOnlyStatus();
    cepats.async(function(){cepats.getFormData();}, null);
	},

  // INITIALIZE

  initEditableItems : function() {

    priorityEdit();
    prioritySave();

    vendorPositionEdit();
    vendorPositionSave();

    vendorsResponseEdit();
    vendorsResponseSave();

    condoCorpPositionEdit();
    condoCorpPositionSave();

    condoCorpResponseEdit();
    condoCorpResponseSave();


    $('table').on( 'draw.dt', function () {
        priorityEdit();
        prioritySave();

        vendorPositionEdit();
        vendorPositionSave();

        vendorsResponseEdit();
        vendorsResponseSave();

        condoCorpPositionEdit();
        condoCorpPositionSave();

        condoCorpResponseEdit();
        condoCorpResponseSave();

         // Disable Table Column Based on User Type
        if(cepats.readOnly){
        	$('table tr td:nth-child(5)').css('pointer-events', 'none');
        }
        if (cepats.userType == "VB" || cepats.readOnly) {
        	$('table tr td:nth-child(8)').css('pointer-events', 'none');
        	$('table tr td:nth-child(9)').css('pointer-events', 'none');
        }
        if (cepats.userType == "CCA" || cepats.readOnly) {
        	$('table tr td:nth-child(6)').css('pointer-events', 'none');
        	$('table tr td:nth-child(7)').css('pointer-events', 'none');
        }
        
    });

    function priorityEdit() {
      $('.formItem__priority a').editable({
          mode: 'inline',
          emptytext: 'Select Priority',
          showbuttons: false
      });
    }
  	 
  	function prioritySave() {
    	$('.formItem__priority a').on('save', function(e, params) {
          var id = $(this).closest('tr').data('id');
          var item = cepats.getItemFromDataArray(id);
          item.priority = params.newValue;
          cepats.updateItemInline(item);
        });
    }
  	

    function vendorPositionEdit() {
      $('.formItem__vendorsPosition a').editable({
        mode: 'inline',
        emptytext: 'Select position',
  	  onblur: "submit",
        showbuttons: false
      });
    }

    function vendorPositionSave() {
      $('.formItem__vendorsPosition a').on('save', function(e, params) {
        var id = $(this).closest('tr').data('id');
        var item = cepats.getItemFromDataArray(id);
        item.vendorsPosition = params.newValue;
        cepats.updateItemInline(item);
      });
    }

    function vendorsResponseEdit() {
      $('.formItem__vendorsResponse a').editable({
        mode: 'inline',
        emptytext: 'Enter text',
  			// savenochange: true,
  			onblur: "submit",
        showbuttons: false
      });
    }

    function vendorsResponseSave() {
      $('.formItem__vendorsResponse a').on('save', function(e, params) {
        var id = $(this).closest('tr').data('id');
        var item = cepats.getItemFromDataArray(id);
        item.vendorsResponse = params.newValue;
        cepats.updateItemInline(item);
      });
    }

    function condoCorpPositionEdit() {
      $('.formItem__condoCorpPosition a').editable({
        mode: 'inline',
        emptytext: 'Select position',
        showbuttons: false
      });
    }

    function condoCorpPositionSave() {
      $('.formItem__condoCorpPosition a').on('save', function(e, params) {
        var id = $(this).closest('tr').data('id');
        var item = cepats.getItemFromDataArray(id);
        item.condoCorpPosition = params.newValue;
        cepats.updateItemInline(item);
      });
    }

    function condoCorpResponseEdit() {
      $('.formItem__condoCorpResponse a').editable({
        mode: 'inline',
        emptytext: 'Enter text',
  			// savenochange: true,
  			onblur: "submit",
        showbuttons: false
      });
    }

    function condoCorpResponseSave() {
      $('.formItem__condoCorpResponse a').on('save', function(e, params) {
        var id = $(this).closest('tr').data('id');
        var item = cepats.getItemFromDataArray(id);
        item.condoCorpResponse = params.newValue;
        cepats.updateItemInline(item);
      });
    }
    
    
    $('.formItem__fundingAndDescription a').on('save', function(e, params) {
    	var id = $(this).closest('tr').data('id');
    	var item = cepats.getItemFromDataArray(id);
    	item.fundingAndDescription = params.newValue;
    	cepats.updateItemInline(item);
    });
    
    $('.formItem__fundingAndDescription a').editable({
        mode: 'inline',
        emptytext: 'Enter text',
  			onblur: "submit",
        showbuttons: false
      });
    
  },

  initSearchInputs : function() {
    // Handle search input
    $("input.search").on("keyup", function() {
      $("input.searchByItemId").val("");
      $("input.searchByPageNum").val("");
      $("input.searchByRef").val("");

      var searchBefore = cepats.searchString;
      var search = $(this).val().trim();

      if (search != null && search.length > 2) {
        cepats.searchString = search;
      } else {
        cepats.searchString = null;
      }

      if (searchBefore != search) {
        cepats.async(function() {
          cepats.isFiltered = true;

          filters.filterResults();
        }, null);
      }
    });

    // Handle search clearing
    $("#searchclear").on("click", function() {
      $("input.search").val("");

      cepats.async(function() {
        filters.clearFilter();
      }, null);
		});

    // Handle search by item ID
    $("input.searchByItemId").on("keyup", function(){
      $("input.searchByRef").val("");
      $("input.searchByPageNum").val("");
      $("input.search").val("");

      cepats.searchString = null;
      cepats.searchItemId = $(this).val().trim();

      if (cepats.searchItemId.length > 0) {
        cepats.async(function() {
          filters.filterByItemId();
        }, null);
      } else {
        cepats.async(function() {
          filters.clearFilter();
        }, null);
      }
    });

    // Handle search by item ID clear
    $("#searchByItemIdClear").on("click", function() {
      $("input.searchByItemId").val("");

      cepats.async(function(){filters.clearFilter();}, null);
    });

    // Handle search by Ref #
    $("input.searchByRef").on("keyup", function(){
    	$("input.searchByItemId").val("");
    	$("input.searchByPageNum").val("");
    	$("input.search").val("");

      if ($(this).val() === "") {
        cepats.async(function() {
          filters.clearFilter();
        }, null);
      } else {
        cepats.searchString = null;
        cepats.searchRefNum = $(this).val().trim();

        cepats.async(function() {
          filters.filterByRefNum();
        }, null);
      }
    });

    // Handle search by Ref #
    $("#searchByRefClear").on("click", function() {
      $("input.searchByRef").val("");

      cepats.async(function() {
        filters.clearFilter();
      }, null);
    });

    // Handle search by page #
    $("input.searchByPageNum").on("keyup", function(){
      $("input.searchByItemId").val("");
      $("input.searchByRef").val("");
      $("input.search").val("");

      cepats.searchString = null;
      cepats.currentPage = $(this).val().trim();

      if (cepats.currentPage == '') {
        cepats.currentPage = 0;
      }

      var oTable = $('table').dataTable();
      oTable.fnPageChange( parseInt(cepats.currentPage - 1 , 10));

      cepats.filteredData = cepats.formData;

      // cepats.async(function() {
      //   cepats.populateFormTable(false);
      // }, null);
    });

    $("#itemsPerPageBtn").on("change", function(){
      $("input.searchByPageNum").val("");
      $("input.searchByItemId").val("");
      $("input.searchByRef").val("");
      $("input.search").val("");

      cepats.searchString = null;

      cepats.itemsPerPage = $( this ).val();

      if (cepats.itemsPerPage == 'all') {
        $('table').DataTable().page.len(-1).draw();
      } else {
        $('table').DataTable().page.len(cepats.itemsPerPage).draw();
      }

      cepats.filteredData = cepats.formData;

      // cepats.async(function() {
      //   cepats.populateFormTable(false);
      // }, null);
    });

    // Handle search by page # clear
    $("#searchByPageNumClear").on("click", function() {
      $("input.searchByPageNum").val("");

      cepats.async(function() {
        filters.clearFilter();
      }, null);
    });
	
	// Resize Text Area on Link Click
	$(document).on("focus", "textarea", function () {	
		$(this).height(1);
		$(this).height(this.scrollHeight); 
    });
	
	// Resive Text Area on Key up
	$(document).on("keyup", "input[type=text], textarea", function () { 
		$(this).height(1);
		$(this).height(this.scrollHeight); 	
	});
	
  },
  // Handle creatw new item
  createNewItem: function() {
	  console.log("createNewItem");
    this.openUpdateModal(null);
  },


  setupPatsUploadForm: function(){
    // $("#uploadForm").bootstrapValidator({
    //   live: "enabled",
    //   feedbackIcons: {
    //     valid: 'glyphicon glyphicon-ok',
    //     invalid: 'glyphicon glyphicon-remove',
    //     validating: 'glyphicon glyphicon-refresh'
    //   },
    //   fields: {
    //     uploadedFile: {
    //       validators: {
    //         notEmpty: {
    //           message: "File is required"
    //         },
    //         file: {
    //           maxSize: 10*1024*1024, //10 mb
    //           extension: "xlsx",
    //           type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    //           message: "Please use the PATS Excel Template provided (File size cannot exceed 10MB)."
    //         }
    //       }
    //     }
    //   }
    // })

		$(document).on('submit', '#uploadForm', function(e) {
			e.preventDefault();

			//var iframe = $("<iframe name=\"uploadIframe\" style=\"position: absolute;top:-1000px;left:-1000px;\"/>").appendTo("body");

			var iframe = document.createElement("iframe");
			iframe.name = 'uploadIframe';
			iframe.style.position = "absolute";
			iframe.style.top = "-1000px";
			iframe.style.left = "-1000px";
			iframe.id = 'uploadIframe';
			document.body.appendChild(iframe);

			this.target = 'uploadIframe';
			this.action = cepats.basePath + cepats.formType + "/" + cepats.enrolmentNumber + "/uploadPATS";

			$("#uploadFormBtn").attr("disabled", "disabled");

			this.submit();

			cepats.removeImportModal();
			cepats.showWaitModal();

			$("#uploadIframe").on('load', function() {
				cepats.removeWaitModal();

				var jsonResultString = $("#uploadIframe").contents().text();
				var result = $.parseJSON(jsonResultString);

        var template;
        var uploadResultModal = $("<div id='uploadResultModal' class='modal fade'>");
        uploadResultModal.appendTo("body");
        uploadResultModal.modal();

        if (result.error) {
          template = cepats.getTemplate("upload.emailbody.modal.template");
          uploadResultModal.html(template(result));
          uploadResultModal.find(".print").on("click", function(){

          $(".printable").html($(".modal-body").html());
            window.print();
          });

          uploadResultModal.on("hidden.bs.modal", function(){
            uploadResultModal.remove();
          });
        } else {
          template = cepats.getTemplate("upload.result.modal.template");
          uploadResultModal.html(template(result));

          uploadResultModal.on("hidden.bs.modal", function() {
            $(".pagination").each(function(i,e) {
              $(this).empty();
            });

            $(".tableContainer").html("<div class='loading'>Loading...</div>");

            cepats.async(function(){cepats.getFormData();}, null);

            uploadResultModal.remove();
          });

          // cepats.addDeletePATSButton();

          var input = $(".fileInput");
          input.replaceWith(input.val('').clone(true));
       	}

        $("#uploadIframe").remove();
      });
    });
	},

  updateItemInline: function(item) {
    $.ajax({
      type: 'POST',
      url: cepats.basePath + cepats.formType + '/updateItem',
      data: JSON.stringify(item),
      async: false,
      dataType: 'json',
      contentType: 'application/json',

      success: function (response) {
        if(!response.error){
          console.log('The item was updated.')
        }else{
          // TODO trigger modal to notify user of failure
          console.log('There was an error updating the item.')
        }
      },

      error: function(xhr, statusText, errorThrown) {
        if(xhr.status == 200){
          location.reload();
        }else{
          console.log('There was an error updating the item.')
        }
      },
    });
  },

  getFormData: function(){
    $.ajax({
      type: "GET",
      async: true,
      cache: false,
      url: $(location).attr("pathname") + "getForm",

      success: function(response) {
        cepats.formData = response.items;
		cepats.isCondoConversionForm = response.isCondoConversionForm;
        cepats.isFiltered = false;
        cepats.userType = response.userType;

        filters.filterResults();

        cepats.currentPage = 1;

        // Allow individual items to be deleted by Tarion users
        if(cepats.userType == "TARION"){
          $(".deleteFormItem").removeClass("hidden");
        }

        // cepats.addDeletePATSButton();
        cepats.initEditableItems();
      },

      error: function(xhr, statusText, errorThrown){
        if (xhr.status == 200) {
          location.reload();
        } else {
          $(".tableContainer").html(errorThrown.status + " - " + errorThrown.statusText);
        }
      },

      dataType: 'json'
    });
  },

  populateFormTable: function(){
    if(this.formData != null && this.formData.length) {
      if(this.filteredData != null && this.filteredData.length) {
        var pageData = this.getPageData(this.currentPage);
        var form = new Object();
        form.items = pageData;
        form.isCondoConversionForm = cepats.isCondoConversionForm;
        var formTableTemplate = this.getTemplate("pats.table.template");

        $(".tableContainer").html(formTableTemplate(form));
        this.registerTableHandlers();
        $("table").DataTable({
          "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
          "bFilter": false,
          "lengthChange": false,
          "pageLength": 50,
          "bInfo" : false,
          "pagingType": "full_numbers_no_ellipses"
       
         });

         // Disable Table Column Based on User Type
        if(cepats.readOnly){
        	$('table tr td:nth-child(5)').css('pointer-events', 'none');
        }
        if (cepats.userType == "VB" || cepats.readOnly) {
        	$('table tr td:nth-child(8)').css('pointer-events', 'none');
        	$('table tr td:nth-child(9)').css('pointer-events', 'none');
        }
        if (cepats.userType == "CCA" || cepats.readOnly) {
        	$('table tr td:nth-child(6)').css('pointer-events', 'none');
        	$('table tr td:nth-child(7)').css('pointer-events', 'none');
        }
        
        // Display the current page
        this.showPage(this.currentPage);

        // Initiate editable from items
        cepats.initEditableItems();
		
    		// Popuplate user selected Priority
    		if(cepats.deficiencyKeywords.length != 0 ){
    			$.fn.wrapInTag = function(opts) {
    			  var tag = opts.tag || 'strong',
    				  words = opts.words || [],
    				  regex = RegExp(words.join('|'), 'gi'),
    				  replacement = '<'+ tag +'>$&</'+ tag +'>';
    			  
    			  return this.html(function() {
    				return $(this).text().replace(regex, replacement);
    			  });
    			};
    			
    			$('table td:nth-child(4)').wrapInTag({
    			  tag: 'b',
    			  words: cepats.deficiencyKeywords 
    			});
    		}
    		
    		// Add added keywords
    		$.each(cepats.deficiencyKeywords, function(i, f) {
    			html = '<button class="keywords-button-li" id="'+cepats.deficiencyKeywords[i]+'">' + cepats.deficiencyKeywords[i] + '</button>';
    			$('#filter-kw').append(html);
    		});
    		
    		// Popuplate user selected Priority
    		if(cepats.priority.length != 0 ){
    			$.each(cepats.priority, function(i, checkedPosition){ 
    				$('#filterByPriority [data-id="'+checkedPosition+'"] input:checkbox').prop('checked', true);
    			});
    		}
    		
    		// Popuplate user selected Vendor Positions
    		if(cepats.vendorsPosition.length != 0 ){
    			$.each(cepats.vendorsPosition, function(i, checkedPosition){ 
    				$('#filterByVBPosition [data-id="'+checkedPosition+'"] input:checkbox').prop('checked', true);
    			});
    		}
    		
    		// Popuplate user selected Condo Corp positions
    		if(cepats.condoCorpPosition.length != 0 ){
    			$.each(cepats.condoCorpPosition, function(i, checkedPosition){ 
    				$('#filterByCondoCorpPosition [data-id="'+checkedPosition+'"] input:checkbox').prop('checked', true);
    			});
    		}

      } else {
        $(".tableContainer").empty();
        $(".tableContainer").html("<div class='loading'>No items match your search criteria.</div>");
        $(".tableContainer").append("<div class='loading'><button type='button' id='clearFilterButton' class='btn btn-primary'>Clear Filter</button></div>");

        $(".pagination").each(function(i,e) {
          $(this).empty();
        });
      }

			// Show form action controls for Tarion users
			if (cepats.userType === "TARION") {
				$("#addItem").removeClass('hidden');
				$("#deletePATSBtn").removeClass('hidden');
				$(".deleteFormItem").removeClass("hidden");
			}

			// Show form action available to all users
			$('#exportPATSForm').removeClass('hidden');
			$("#importPATSForm").removeClass('hidden');
			
			$('#exportPATSTemplate').addClass('hidden');
			

    } else {
      $(".tableContainer").empty();
      $(".tableContainer").html("<div class='loading'>No PATS has been uploaded. Please upload your PATS</div>");

      $(".pagination").each(function(i,e) {
        $(this).empty();
      });

			// Show form action controls available to all users when form not populated
			$("#importPATSForm").removeClass('hidden');
			$('#exportPATSTemplate').removeClass('hidden');

			// Hide form action controls that don't apply without a populated form
			$("#addItem").addClass('hidden');
			$("#deletePATSBtn").addClass('hidden');
			$(".deleteFormItem").addClass("hidden");
			$('#exportPATSForm').addClass('hidden');
    }

  },

  getPageData : function(pageNum) {
    // if (this.itemsPerPage == this.filteredData.length) {
    //   this.currentPage = 1;
    // } else {
    //   cepats.currentPage = pageNum;
    // }

    // var lower = (pageNum-1) * cepats.itemsPerPage;
    // var upper = pageNum * cepats.itemsPerPage;
    var pageData = new Array();

    for (i = 0; i < this.filteredData.length; i++) { 
        if(this.filteredData[i] != null) {
        pageData.push(this.filteredData[i]);
      }
    }

    return pageData;
  },

  showPage: function(pageNum) {
		// var itemCount = this.filteredData.length;
		// var pages = Math.ceil(itemCount/cepats.itemsPerPage);

		// if (itemCount > cepats.itemsPerPage) {
  //     var paginationTemplate = cepats.getTemplate("pagination.template");

  //     $(".pagination").each(function(i,e) {
  //       $(this).html(paginationTemplate({pagination: {
  //         page: pageNum,
  //         pageCount: pages
  //       }}));
  //     });

  //     $(".pagination a").on("click", function(e) {
  //       e.preventDefault();

  //       cepats.currentPage = $(this).attr("data-pageNum");
  //       cepats.populateFormTable();
  //     });

  //   } else {
  //     $(".pagination").each(function(i,e) {
  //       $(this).empty();
  //     });
  //   }
	},

  registerTableHandlers: function() {
    $(".tableContainer button.versionHistory").on("click", function(){
      cepats.openHistoryModal($(this).parent().parent().parent().attr("data-id"));
    });

    $(".tableContainer button.attachments").on("click", function(){
      cepats.openAttachmentsModal($(this).parent().parent().parent().attr("data-id"));
    });

    $(".tableContainer button.update").on("click", function(){
    cepats.openUpdateModal($(this).parent().parent().parent().attr("data-id"));
    });
  },

  openUpdateModal: function(id) {
    var template = this.getTemplate("update.item.modal.template");
    var updateModal = $("<div id='historyModal' class='modal fade'>");

    updateModal.appendTo("body");
    updateModal.modal();

    var data;

    if (id != null) {
      data = this.getItemFromDataArray(id);
      this.openItem = data;
    } else {
      this.openItem = {};
      data = null;
    }

		var templateData = {};
		templateData.itemData = data;
		templateData.userInfo = cepats.user;

		updateModal.html(template(templateData));

		updateModal.find("form").bootstrapValidator({
      live: "enabled",
      trigger: "blur",
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        auditRefNum: {
          validators: {
            notEmpty: {
              message: "The PA Ref. # is a mandatory field. Please complete this field"
            },
            stringLength: {
              max: 95,
              message: "The PA Ref. # cannot exceed 95 characters. Please correct"
            }
          }
        },
        deficiencyDescription: {
          validators: {
            notEmpty: {
              message: "The Deficiency Description is a mandatory field. Please complete this field"
          },
          stringLength: {
            max: 1400,
            message: "The Deficiency Description cannot exceed 1400 characters. Please correct"
          }
          }
        },
        deficiencyLocation: {
          validators: {
            notEmpty: {
              message: "The Deficiency Location is a mandatory field. Please complete this field"
            },
            stringLength: {
              max: 1400,
              message: "The Deficiency Location cannot exceed 1400 characters. Please correct"
            }
          }
        },
        deficiencyLocation: {
            validators: {
              stringLength: {
                max: 200,
                message: "The PEF cannot exceed 200 characters. Please correct"
              }
            }
          }
      //   vendorsResponse: {
      //     validators: {
      //       stringLength: {
      //         max: 5000,
      //         message: "The Vendor's Response cannot exceed 5000 characters. Please correct."
      //       }
      //     }
      //   },
      //   condoCorpResponse: {
      //     validators: {
      //       stringLength: {
      //         max: 5000,
      //         message: "The Condo Corp Response cannot exceed 5000 characters. Please correct."
      //       }
      //     }
      //   }
      }
    }).on('success.form.bv', function(e){
      e.preventDefault();

      cepats.openItem.auditRefNum = $(updateModal).find("input[name=auditRefNum]").val();
      cepats.openItem.deficiencyDescription = $(updateModal).find("textarea[name=deficiencyDescription]").val();
      cepats.openItem.deficiencyLocation = $(updateModal).find("textarea[name=deficiencyLocation]").val();
      cepats.openItem.vendorsResponse = $(updateModal).find("textarea[name=vendorsResponse]").val();
      cepats.openItem.condoCorpResponse = $(updateModal).find("textarea[name=condoCorpResponse]").val();
      cepats.openItem.priority = $(updateModal).find("select[name=priority]").val();
      cepats.openItem.vendorsPosition = $(updateModal).find("select[name=vendorsPosition]").val();
      cepats.openItem.condoCorpPosition = $(updateModal).find("select[name=condoCorpPosition]").val();
      cepats.openItem.enrolmentNumber = cepats.enrolmentNumber;

      console.log(JSON.stringify(cepats.openItem));
      var url;

      if(cepats.openItem.id != undefined) {
        url = cepats.basePath + cepats.formType +  '/updateItem';
      } else {
        //TODO see if a separate method is required
        url = cepats.basePath + cepats.formType + '/updateItem';
      }

      $.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(cepats.openItem),
        dataType: 'json',
        async: false,

        success: function (response) {
          if(!response.error) {
            cepats.replaceItemInArray(cepats.openItem);

            cepats.searchString == null;
            cepats.isFiltered = false;

            filters.filterResults();

            updateModal.modal('hide');
          } else {

            var error = $("<span class='pull-left' style='color: #FF0000;'>An error occurred while submitting the item. Please try again later.</span>")
            updateModal.find(".modal-footer").prepend(error);
          }
        },

        error: function(xhr, statusText, errorThrown) {
          if (xhr.status == 200) {
            location.reload();
          } else {
            var error = $("<span class='pull-left' style='color: #FF0000;'>An error occurred while submitting the item. Please try again later.</span>")
            updateModal.find(".modal-footer").prepend(error);
          }
        },

        failure: function(response) {
          var error = $("<span class='pull-left' style='color: #FF0000;'>An error occurred while submitting the item. Please try again later.</span>")
          updateModal.find(".modal-footer").prepend(error);
        },

        dataType: 'json',
        contentType: 'application/json'
      });
    });

    updateModal.on("hidden.bs.modal", function() {
      updateModal.remove();
      cepats.openItem = null;
    });
  },



  downloadHistory: function() {
    
      fromDate = $('#from-date input').val();
      toDate = $('#to-date input').val();

      if(fromDate == ""){
        alert("Please enter initial date.");
      } else if (toDate == ""){
        alert("Please enter end date.");
      } else {
        $.ajax({
          type: "GET",
          async: false,
          url: cepats.basePath + "downloadHistory/" + cepats.enrolmentNumber + "/" + cepats.formType + "/" + fromDate + "/" + toDate,
          cache: false,

          success: function(response){
            console.log(response);

            $('.pdf').show();
            $('.pdf').append(response);

            var table = $('.pdf table').tableToJSON();

            var columns = [    
                {title: "Item #", dataKey: "Item #"},
                {title: "Changes", dataKey: "Changes"},
                {title: "Changes Made", dataKey: "Changes Made"}, 
                {title: "Revised By", dataKey: "Revised By"}, 
                {title: "Revised On", dataKey: "Revised On"},
            ];

            var rows = table;

            var doc = new jsPDF();
             doc.text(15, 10, cepats.enrolmentNumber);
             doc.autoTable(columns, rows, {
               // Styling
              theme: 'striped',
              styles: {
                overflow: 'linebreak'
              },
              columnStyles: {
                "Item #": {columnWidth:20},
                "Changes": {columnWidth: 50},
                "Changes Made": {columnWidth: 'auto'},
                "Revised By": {columnWidth: 30},
                "Revised On": {columnWidth: 30}
              },
              showHeader: 'firstPage' // 'everyPage', 'firstPage', 'never'
            });
            doc.save('history.pdf');

            $('.pdf').empty();
            $('.pdf').hide();

          },

          error: function(xhr, statusText, errorThrown) {
            if (xhr.status == 200) {
              location.reload();
            }

            console.log(errorThrown.status + " - " + errorThrown.statusText);
          }
        });
      }
  },

  openHistoryModal: function(itemId){
    var template = this.getTemplate("history.modal.template");
    var historyModal = $("<div id='historyModal' class='modal fade'>");

    historyModal.appendTo("body");
    historyModal.modal();

    $.ajax({
      type: "GET",
      async: false,
      url: cepats.basePath + "getItemHistory/"  + cepats.enrolmentNumber + "/" +  itemId,
      cache: false,

      success: function(response) {
		
        historyModal.html(template(response));

        historyModal.find("button.print").on("click", function() {
          $(".printable").html($(".modal-body").html());
          window.print();
        });

        historyModal.on("hidden.bs.modal", function() {
          historyModal.remove();
        });
      },

      error: function(xhr, statusText, errorThrown) {
        if (xhr.status == 200) {
          location.reload();
        } else {
          var error = $("<span class='pull-left' style='color: #FF0000;'>An error occurred while retrieving the history. Please try again later.</span>")
          historyModal.find(".modal-footer").prepend(error);
        }
      }
    });
  },

  openAttachmentsModal: function(itemId) {
    var template = this.getTemplate("attachments.modal.template");
    var attachmentsModal = $("<div id='attachmentsModal' class='modal fade'>");

    attachmentsModal.appendTo("body");
    attachmentsModal.modal();
    
    $.ajax({
      type: "GET",
      async: false,
      cache: false,
      url: cepats.basePath + "getAttachments/" + itemId,

      success: function(response) {
        var data = {
          id: itemId,
          attachments: response
        };

        attachmentsModal.html(template(data));

        attachmentsModal.on("hidden.bs.modal", function(){
          attachmentsModal.remove();
        });
        
        
        if(cepats.readOnly){
    		$(".addAttachmentUploadForm").remove();
    		$("#uploadAttachment").remove();
    	}

        attachmentsModal.find("form").bootstrapValidator({
          live: "enabled",
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            attachment: {
              validators: {
                notEmpty: {
                  message: "File is required"
                },
                file: {
                  maxSize: 25*1024*1024, //25 mb
                  extension: "jpeg,jpg,pdf,doc,docx",
                  type: "image/jpeg,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                  message: "You can upload only in one of the following formats: doc, docx, pdf, jpg and jpeg (File size cannot exceed 25MB)."
                }
              }
            }
          }
        })
        .on('success.form.bv', function(e) {
          e.preventDefault();

          var input = attachmentsModal.find("input");
          var id = input.parent().attr("data-id");
          var formData = new FormData();
          var file = $("[name='attachment']").prop("files")[0];

          formData.append("file", file);

          $.ajax({
            url: cepats.basePath + cepats.formType + "/" + cepats.enrolmentNumber  + "/uploadAttachment/" + id,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,

            success: function(response) {
              var tableRow = 'tr[data-id=' + id + ']';

              $(tableRow).find('button.attachments').addClass('active').addClass('red');

              attachmentsModal.modal('hide');
              cepats.openAttachmentsModal(id);
            },

            error: function(xhr, statusText, errorThrown) {
              if(xhr.status == 200) {
                location.reload();
              } else {
                var error = $("<span class='pull-left' style='color: #FF0000;'>An error occurred while uploading your file. Please try again later.</span>")
                attachmentsModal.find(".modal-footer").prepend(error);
              }
            },

            failure: function(jqXHR, textStatus, errorThrown) {
              var error = $("<span class='pull-left' style='color: #FF0000;'>An error occurred while uploading your file. Please try again later.</span>")
              attachmentsModal.find(".modal-footer").prepend(error);
            }
          });
        });
      },

      error: function(xhr, statusText, errorThrown) {
        if(xhr.status == 200) {
          location.reload();
        } else {
          var error = $("<span class='pull-left' style='color: #FF0000;'>An error occurred while retrieving the attachments list. Please try again later.</span>")
          attachmentsModal.find(".modal-footer").prepend(error);
        }
      }
    });
  },

  getItemFromDataArray: function(itemId) {
    if (this.formData != null && this.formData.length > 0) {

      for (var i=0;i<this.formData.length;i++) {

        if (this.formData[i].id == itemId) {
          return this.formData[i];
        }
      }
    }

    return null;
  },

  replaceItemInArray: function(item){
    if(item.id != undefined){
      var replaceIndex;

      for(var i=0;i<this.formData.length;i++){

        if(item.id == this.formData[i].id) {
          replaceIndex = i;
          break;
        }
      }

      this.formData[replaceIndex] = item;

    } else {
      $(".tableContainer").empty();
      $("<div class=\"loading\"><br/><br/>Loading PATS Form...</div>").appendTo(".tableContainer");

      cepats.async(function(){cepats.getFormData();}, null);
    }
  },

  exportForm: function(){
    var requestUrl = cepats.basePath + cepats.formType + "/" + cepats.enrolmentNumber   + "/exportForm";

    $("<iframe style='display:none;' src='" + requestUrl + "' class='exportIframe' />").appendTo("body");

    this.showWaitModal();

    setTimeout(function() {
      cepats.removeWaitModal();
      $("iframe.exportIframe").remove();
    }, 3000);
  },

  exportTemplate: function(){
    // $.ajax({
    //   type: "GET",
    //   url: cepats.basePath + 'downloadTemplate',
    //   async: true,
    //   cache: false,
    //   success: function(response) {
    //     console.log(response);
    //   },
    //   error: function(xhr, statusText, errorThrown) {
    //     if (xhr.status == 200) {
    //       location.reload();
    //     }
    //   }
    // });

    // var requestUrl = cepats.basePath + "files/PATS-template.xlsx";
    // $("<iframe style='display:none;' src='" + requestUrl + "' class='exportIframe' />").appendTo("body");
    // setTimeout(function(){
    //   $("iframe.exportIframe").remove();
    // }, 1000);
  },

    showWaitModal: function() {
      if(!$("#waitModal").length > 0){
        var template = cepats.getTemplate("wait.modal.template");

        $("<div id=\"waitModal\" class=\"modal fade\" />").appendTo("body");

        $("#waitModal").html(template(null)).modal({backdrop: 'static', keyboard: false});
      }
    },

    removeWaitModal: function() {
      $("#waitModal").modal('hide');
      $("#waitModal").remove();
      $(".modal-backdrop").remove();
    },

    getUserInformation : function() {
      $.ajax({
        type: "GET",
        url: cepats.basePath + cepats.enrolmentNumber + "/" + "userInformation",
        async: true,
        cache: false,

        success: function(response) {
          cepats.user = response;
          cepats.enrolmentNumber = response.enrolment.enrolmentNumber;
          cepats.userType = response.userType;

          if (response.userType === "TARION") {
            $("#deletePATSBtn").removeClass("hidden");
            $(".deleteFormItem").removeClass("hidden");
          }

          if (cepats.formType === "firstyear" && response.firstYearFormSubmitted === false || cepats.formType === "secondyear" && response.secondYearFormSubmitted === false) {
            $('#exportPATSTemplate').removeClass('hidden');
          }

        	if (response.userType == "VB"
            || (response.userType != "TARION" && cepats.formType == "firstyear" && (!response.withinFirstYearPeriod || !response.firstYearFormSubmitted))
            || (response.userType != "TARION" && cepats.formType == "secondyear" && (!response.withinSecondYearPeriod || !response.secondYearFormSubmitted))) {
          } else {
            $("button.createNew").removeClass('hidden');
            $("button.createNew").on("click", function() {
                cepats.createNewItem();
            });
          }


        },

        error: function(xhr, statusText, errorThrown) {
          if(xhr.status == 200){
            location.reload();
          }
        }
      });
    },

    //get and compile ajax template
    getTemplate: function(templateName) {
      if(cepats.templates[templateName] == undefined){
        var template = "";

        $.ajax({
          url: cepats.basePath + 'js/templates/' + templateName,
          cache: true,
          async: false,

          success: function(data) {
            template = Handlebars.compile(data);
            cepats.templates[templateName] = template;
          },

          error: function(xhr, statusText, errorThrown) {
            if (xhr.status == 200) {
              location.reload();
            }
          }
        });

        return template;
      }

      return cepats.templates[templateName];
    },

    async : function(func, callback) {
      setTimeout(function() {
      func();

      if(callback) {
        callback();
      }
    }, 0);
  },

	showConfirmDeletePATSModal: function() {
	   var year = '';
	   if (this.formType == "firstyear") {
		   year = "First Year";
	   } else if (this.formType == "secondyear") {
		   year = "Second Year";
	   }

	   var data = { formType : year, enrolmentNumber : this.enrolmentNumber};
	   var template = cepats.getTemplate("confirmDeletePATS.modal.template");

     $("<div id=\"confirmDelete\" class=\"modal fade\" />").appendTo("body");

	   $("#confirmDelete").html(template(data)).modal({backdrop: 'static', keyboard: false});

		 $(document).on("click", "#confirmlDeletePATS", function() {
 			cepats.deletePATS();
 		});
  },

	removeConfirmDeletePATSModal: function(){
		$("#confirmDelete").modal('hide');
		$("#confirmDelete").remove();
		$(".modal-backdrop").remove();
	},

  deletePATS : function() {
    $.ajax({
      url: cepats.basePath + 'deletePATS/' + cepats.enrolmentNumber + '/' + cepats.formType,
      cache: true,
      async: false,

      success: function(data) {
        if(data != "success"){
          cepats.displayError();
        }

        cepats.getFormData();

        $("#deletePATSBtn").addClass('hidden');
		$('#exportPATSForm').addClass('hidden');

        cepats.removeConfirmDeletePATSModal();
      },

      error: function(xhr, statusText, errorThrown) {
        if (xhr.status == 200) {
          location.reload();
        }

        $('#deletePATSResult').addClass('error');
        $('#deletePATSResult').html('An error occurred while deleting PATS. Please try again later.');
      }
    });
  },

  showConfirmDeleteItemModal: function(formItem) {
    console.log(formItem);
    var itemNum = $(formItem).find('.formItem__itemId').html();
    console.log(itemNum);
    var data = { itemNum : itemNum, enrolmentNumber : this.enrolmentNumber};
    console.log(data)
    var template = cepats.getTemplate("confirmDeleteItem.modal.template");

    $("<div id=\"confirmDelete\" class=\"modal fade\" />").appendTo("body");

    $("#confirmDelete").html(template(data)).modal({backdrop: 'static', keyboard: false});

    $("#confirmlDeleteItem").on("click", function(){
      cepats.deleteFormItem(formItem);
    });
  },

  removeConfirmDeleteItemModal : function(){
    $("#confirmDelete").modal('hide');
    $("#confirmDelete").remove();
		$(".modal-backdrop").remove();
  },

  deleteFormItem: function(formItem) {
    var enrolmentNum = cepats.user.enrolment.enrolmentNumber;
    var formType = cepats.formType === 'firstyear' ? 'FIRSTYEAR' : 'SECONDYEAR';
    var itemId = $(formItem).find('.formItem__itemId').html();
    var path = '/' + enrolmentNum + '/' + itemId + '/' + formType;

    $.ajax({
      type: "GET",
      async: true,
      cache: false,
      url: cepats.basePath + 'deleteLineItem' + path,
      context: this,

      success: function(response){
        $(formItem).remove();

        cepats.removeConfirmDeleteItemModal();

        for(i=this.formData.length-1; i>=0; i--) {
          if(this.formData[i].itemId == itemId) {
            this.formData.splice(i,1);
          }
        }
      },

      error: function(xhr, statusText, errorThrown) {
        if (xhr.status == 200) {
          location.reload();
        }

        console.log(errorThrown.status + " - " + errorThrown.statusText);

        cepats.removeConfirmDeleteItemModal();
      }
    });
  },


  addDeletePATSButton : function(){
    if(cepats.userType == "TARION"){
      $('#deletePATSBtn').removeClass('hidden');

      $(document).on("click", "#deletePATSBtn", function(){
        cepats.showConfirmDeletePATSModal();
      });
    }
  },

  openImportModal: function(){
		var template = this.getTemplate("importForm.modal.template");
		var importModal = $("<div id='importModal' class='modal fade'>");

		importModal.appendTo("body");
		importModal.modal();
  	importModal.html(template);

  	importModal.find("form").bootstrapValidator({
      live: "enabled",
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        file: {
          validators: {
            notEmpty: {
              message: "File is required"
            },
            file: {
              maxSize: 25*1024*1024, //25 mb
              extension: "xls,xlsx", // Microsoft Excel spreadsheet
              type: "application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
              message: "You can upload only in one of the following formats: xls, xlsx (File size cannot exceed 25MB)."
            }
          }
        }
      }
    })
    .on('success.form.bv', function(e){
      e.preventDefault();
      $('#uploadFormBtn').removeAttr('disabled');
    });

    importModal.on("hidden.bs.modal", function(){
      importModal.remove();
    });
  },

  removeImportModal: function(){
    $("#importModal").modal('hide');
    $("#importModal").remove();
		$(".modal-backdrop").remove();
  },

  getEnrolmentInfo : function(){
    $.ajax({
      type: "GET",
      url: cepats.basePath + cepats.enrolmentNumber + "/" + "getEnrolmentInfo",
      async: true,
      cache: false,

      success: function(response) {
        if (response.errorMessage === null) {
          // Handle empty response fields
          if (response.wscAssigned === null) { response.wscAssigned = "Data could not be retrieved, please try again later." }
          if (response.wsrAssigned === null) { response.wsrAssigned = "Data could not be retrieved, please try again later." }

          // Append response to enrolment info list
          $('#enrolmentInfo').append('<dt>Warranty Services Coordinator</dt><dd>' + response.wscAssigned + '<br /><a href="mailto:' +  response.wscAssignedEmail + '">' +  response.wscAssignedEmail + '</a></dd>');
          $('#enrolmentInfo').append('<dt>Warranty Services Representative</dt><dd>' + response.wsrAssigned + '<br /><a href="mailto:' +  response.wsrAssignedEmail + '">' +  response.wsrAssignedEmail + '</a></dd>');

          // Loop through timlines if present and append to warranty info list.
          if (response.timelines) {
            var timelineItems = [];
            $.each(response.timelines, function(i, timeline) {
              timelineItems.push('<dt>Timeline: ' + timeline.startDate + ' - ' + timeline.endDate + '</dt>' + '<dd>' + timeline.activityDesc + '</dd>');
            });
            $('#warrantyInfo').append(timelineItems.join(''));
          }

        } else {
          // Show error message
          $('#enrolmentInfoError').removeClass('hidden').html(response.errorMessage);
        }
      },

      error: function(xhr, statusText, errorThrown){
        if(xhr.status == 200){
          location.reload();
        }
      }
    });
  },
  
  getReadOnlyStatus : function(){
	    $.ajax({
	      type: "GET",
	      url: cepats.basePath + "readOnly",
	      async: true,
	      cache: false,
	      success: function(response) {
	    	  if(response == "true"){
	    		  cepats.readOnly = true;
	    	  }else{
	    		  cepats.readOnly = false;
	    	  }
	      },
	      error: function(xhr, statusText, errorThrown){
	        if(xhr.status == 200){
	          location.reload();
	        }
	      }
	    });
	  }
};