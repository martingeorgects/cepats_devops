package com.tarion.cepats.web.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.cepats.entities.FormItemEntity;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;
import com.tarion.cepats.services.FormService;
import com.tarion.cepats.services.PropertyService;
import com.tarion.cepats.services.attachments.AttachmentUploadService;
import com.tarion.cepats.web.dtos.FormDto;
import com.tarion.cepats.web.dtos.FormItemDto;
import com.tarion.cepats.web.dtos.UpdateResultDto;
import com.tarion.cepats.web.dtos.UserDto;
import com.tarion.cepats.web.dtos.UserInfoDto;


@Controller
public class CepatsFormController extends CEPATSController{

    @Autowired
    private FormService formService;
    @Autowired
    private AttachmentUploadService uploadService;
    @Autowired
    private PropertyService propertyService;

    @RequestMapping(value = "/{enrolmentNumber}/", produces = "text/html")
    public String redirect(@PathVariable("enrolmentNumber")String enrolmentNumber, HttpServletRequest request, HttpSession session) {
		LoggerUtil.logClientIp(CepatsFormController.class, "redirect", request);
		UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));
        return CEPATSUtil.findMapping(user, enrolmentNumber);
    }

    @RequestMapping(value = {"/{formType}/{enrolmentNumber}/"}, produces = "text/html")
    public String getIndex(@PathVariable("formType")String formType, @PathVariable("enrolmentNumber")String enrolmentNumber, Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));
    	long startTime = LoggerUtil.logEnter(CepatsFormController.class, "getIndex", user.getUsername(), user.getLoggedinUserType(), 
    			user.getEnrolment().getEnrolmentNumber(),
                user.getEndOfFirstYearWarranty(), user.getEndOfSecondYearWarranty());
                
        populateModel(formType, enrolmentNumber, model, user);
  
		LoggerUtil.logExit(CepatsFormController.class, "getIndex", "patsForm", startTime);
		setNoCacheResponseHeaders(response); 
		return "patsForm";
    }

	private void populateModel(String formType, String enrolmentNumber, Model model, UserDto user) {
		populateModelWithCommonAttributes(formType, enrolmentNumber, model, user);
       
        if(isBuilderUser(user)){
        	populateModelForBuilder(model);
        }else{
        	populatedModelForCondoCorp(model);
        }
	}

	private void populateModelWithCommonAttributes(String formType, String enrolmentNumber, Model model, UserDto user) {
		model.addAttribute("user", user);
        model.addAttribute("formType", formType);
        model.addAttribute("enrolmentNumber", enrolmentNumber);
	}

	private boolean isBuilderUser(UserDto user) {
		return user != null && user.getLoggedinUserType() == UserTypeEnum.VB;
	}

	private void populatedModelForCondoCorp(Model model) {
		model.addAttribute("headerCssClass", "myhomeHeader");
        model.addAttribute("videoTutorialLink", propertyService.getCondoCorpVideoTutorialLink());
        model.addAttribute("tutorialLink",  propertyService.getCondoCorpTutorialLink());
	}

	private void populateModelForBuilder(Model model) {
		model.addAttribute("headerCssClass", "builderlinkHeader");
        model.addAttribute("videoTutorialLink", propertyService.getBuilderVideoTutorialLink());
        model.addAttribute("tutorialLink", propertyService.getBuilderTutorialLink());
	}

    @RequestMapping(value = "/{formType}/{enrolmentNumber}/getForm", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public FormDto getForm(@PathVariable("formType")String formType,  @PathVariable("enrolmentNumber")String enrolmentNumber, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));
        user.setFormType(FormTypeEnum.valueOf(formType.toUpperCase()));
        Long startTime = LoggerUtil.logEnter(CepatsFormController.class, "getForm",
                                             user.getEnrolment().getEnrolmentNumber(), user.getFormType());
		LoggerUtil.logClientIp(CepatsFormController.class, "getForm", request);

		List<FormItemDto> formItems = null;

        try {
            formItems = formService.getItemsByEnrolmentAndType(user.getEnrolment().getEnrolmentNumber(), 
            		user.getFormType(), user.getLoggedinUserType().getLabel().toUpperCase());
        } catch (Exception e) {
            LoggerUtil.logError(CepatsFormController.class, e.getMessage(), e);
        }
        FormDto form = new FormDto();
        form.setItems(formItems);
        form.setIsCondoConversionForm(formService.isCondoConversionForm(user.getEnrolment().getEnrolmentNumber()));
        form.setUserType(user.getLoggedinUserType().toString());
        
        LoggerUtil.logExit(CepatsFormController.class, "getForm", formItems, startTime);
        setNoCacheResponseHeaders(response); 
        return form;
    }

    @RequestMapping(value = "/{formType}/{enrolmentNumber}/uploadPATS", method = RequestMethod.POST, produces = "text/html", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE} )
    public @ResponseBody String uploadPATS(@PathVariable("formType")String formType,
                                           @PathVariable("enrolmentNumber")String enrolmentNumber,
                                           @RequestParam("file") MultipartFile file, HttpSession session, HttpServletRequest request) {

        Long startTime = LoggerUtil.logEnter(CepatsFormController.class, "uploadPATS", formType);
		LoggerUtil.logClientIp(CepatsFormController.class, "uploadPATS", request);
		UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));

		String ret = "";

        try {
            user.setUpdateAsUserType(user.getLoggedinUserType());
            user.setFormType(FormTypeEnum.valueOf(formType.toUpperCase()));
            UpdateResultDto updateResult = formService.upload(file, user);
            updateResult.setFormType(user.getFormType());
            
            ObjectMapper mapper = new ObjectMapper();
            ret = mapper.writeValueAsString(updateResult);
        } catch (Exception e) {
            LoggerUtil.logError(CepatsFormController.class, e.getMessage(), e);
        }

        LoggerUtil.logExit(CepatsFormController.class, "uploadPATS", ret, startTime);
        return ret;
    }

    @RequestMapping(value = "/{formType}/{enrolmentNumber}/saveItem", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public UpdateResultDto saveItem(@PathVariable("formType")String formType,
    		 @PathVariable("enrolmentNumber")String enrolmentNumber, 
                                    @RequestParam(value = "itemData")String itemData,
                                    @RequestParam(value = "attachment", required = false) MultipartFile attachment, 
                                    HttpSession session, HttpServletRequest request) {

        Long startTime = LoggerUtil.logEnter(CepatsFormController.class, "saveItem", formType);
		LoggerUtil.logDebug(CepatsFormController.class, "redirect - client ip", request.getRemoteAddr());
		UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));

		UpdateResultDto updateResult = null;

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            FormItemDto formItemDto = objectMapper.readValue(itemData, FormItemDto.class);
            user.setUpdateAsUserType(user.getLoggedinUserType());
            user.setFormType(FormTypeEnum.valueOf(formType.toUpperCase()));
            updateResult = formService.saveItem(formItemDto, user);
            updateResult.setFormType(user.getFormType());

            if(attachment != null) {
            	FormItemEntity entity = formService.getFormItem(formItemDto.getId()); 
            	uploadService.uploadAttachment(attachment.getOriginalFilename(), attachment.getBytes(), attachment.getContentType(), user, entity);
            }
        } catch (Exception e) {
            LoggerUtil.logError(CepatsFormController.class, e.getMessage(), e);
        }

        LoggerUtil.logExit(CepatsFormController.class, "saveItem", updateResult, startTime);
        return updateResult;
    }
    
    @RequestMapping(value = "/{formType}/updateItem", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody UpdateResultDto updateItem(@PathVariable("formType")String formType, @RequestBody FormItemDto formItemDto, 
    		HttpSession session, HttpServletRequest request){
    	 Long startTime = LoggerUtil.logEnter(CepatsFormController.class, "updateItem", formType, formItemDto);
 		 LoggerUtil.logClientIp(CepatsFormController.class, "updateItem", request);
  		 UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(formItemDto.getEnrolmentNumber()));
    	 UpdateResultDto updateResult = null;
    	 
    	 try {
             user.setUpdateAsUserType(user.getLoggedinUserType());
             user.setFormType(FormTypeEnum.valueOf(formType.toUpperCase()));
             updateResult = formService.saveItem(formItemDto, user);
             updateResult.setFormType(user.getFormType());

         } catch (Exception e) {
             LoggerUtil.logError(CepatsFormController.class, e.getMessage(), e);
         }
    	 
    	 LoggerUtil.logExit(CepatsFormController.class, "updateItem", updateResult, startTime);
    	 return updateResult;
    }
    
    @RequestMapping(value = "/{formType}/{enrolmentNumber}/exportForm", method = RequestMethod.GET)
    public void exportForm(@PathVariable("formType")String formType, @PathVariable("enrolmentNumber")String enrolmentNumber, 
    		HttpSession session, HttpServletResponse response, HttpServletRequest request) {

        Long startTime = LoggerUtil.logEnter(CepatsFormController.class, "exportForm", formType, response);
 		LoggerUtil.logClientIp(CepatsFormController.class, "exportForm", request);
		UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));

        try {

            user.setFormType(FormTypeEnum.valueOf(formType.toUpperCase()));

            StringBuffer fileName = new StringBuffer(user.getEnrolment().getEnrolmentNumber()).append("_");
            fileName.append(formType.toLowerCase()).append(".xlsx");

            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName.toString());
            setNoCacheResponseHeaders(response); 

            formService.export(user).write(response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
            LoggerUtil.logError(CepatsFormController.class, e.getMessage(), e);
        }

        LoggerUtil.logExit(CepatsFormController.class, "exportForm", startTime);
    }
    
    @RequestMapping(value = "/{enrolmentNumber}/downloadTemplate", method = RequestMethod.GET)
    public void downloadTemplate(@PathVariable("enrolmentNumber")String enrolmentNumber, 
    		HttpServletResponse response, HttpServletRequest request, HttpSession session) {

        Long startTime = LoggerUtil.logEnter(CepatsFormController.class, "downloadTemplate",response);
 		LoggerUtil.logClientIp(CepatsFormController.class, "downloadTemplate", request);

 		UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));
        try {

        	StringBuffer fileName = new StringBuffer("PATS-template.xlsx");

            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName );
            setNoCacheResponseHeaders(response); 
            formService.getTemplate(user).write(response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
            LoggerUtil.logError(CepatsFormController.class, e.getMessage(), e);
        }

        LoggerUtil.logExit(CepatsFormController.class, "downloadTemplate", startTime);
    }

    @RequestMapping(value = "/{enrolmentNumber}/userInformation", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public UserInfoDto getUserInformation(@PathVariable("enrolmentNumber")String enrolmentNumber, HttpServletRequest request, HttpSession session) {

    	Long startTime = LoggerUtil.logEnter(CepatsFormController.class, "getUserInformation");
 		LoggerUtil.logClientIp(CepatsFormController.class, "getUserInformation", request);
		UserDto user = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));

        UserInfoDto userInfoDto = new UserInfoDto();
        try {
            userInfoDto.setWithinFirstYearPeriod(CEPATSUtil.isUnderWarranty(FormTypeEnum.FIRSTYEAR, user));
            userInfoDto.setWithinSecondYearPeriod(CEPATSUtil.isUnderWarranty(FormTypeEnum.SECONDYEAR, user));
            userInfoDto.setUserType(user.getLoggedinUserType());
            userInfoDto.setPriority(CEPATSConstants.priorityValues);
            userInfoDto.setCondoPosition(CEPATSConstants.condoPositionValues);
            userInfoDto.setVendorPosition(CEPATSConstants.vendorPositionValues);
            userInfoDto.setEnrolment(user.getEnrolment());
            userInfoDto.setFirstYearFormSubmitted(formService.isFormSubmitted(user.getEnrolment().getEnrolmentNumber(), FormTypeEnum.FIRSTYEAR));
            userInfoDto.setSecondYearFormSubmitted(formService.isFormSubmitted(user.getEnrolment().getEnrolmentNumber(), FormTypeEnum.SECONDYEAR));
        } catch (Exception e) {
            LoggerUtil.logError(CepatsFormController.class, e.getMessage(), e);
        }

        LoggerUtil.logExit(CepatsFormController.class, "getUserInformation", userInfoDto, startTime);
        return userInfoDto;
    }
    
	@RequestMapping(value = "/deletePATS/{enrolmentNumber}/{formType}", produces = "text/html")
	@ResponseBody
	public String deletePATS(@PathVariable("enrolmentNumber") String enrolmentNumber, @PathVariable("formType") String formType,
			HttpServletRequest request, HttpSession session, HttpServletResponse response) {
		try {
			formService.deletePATS(enrolmentNumber, formType);
		} catch (Exception e) {
			setNoCacheResponseHeaders(response); 
			return "error";
		}
		setNoCacheResponseHeaders(response); 
		return "success";
	}
	
	@RequestMapping(value = "/getCondoConversionFlag/{enrolmentNumber}", produces = "text/html")
	@ResponseBody
	public boolean getCondoConversionFlag(@PathVariable("enrolmentNumber") String enrolmentNumber,
			HttpServletRequest request, HttpSession session, HttpServletResponse response) {
		boolean isCondoConversion = false;
		try {
			isCondoConversion =  formService.isCondoConversionForm(enrolmentNumber);
		} catch (Exception e) {
			setNoCacheResponseHeaders(response); 
			return false;
		}
		setNoCacheResponseHeaders(response); 
		return isCondoConversion;
	}
	
	@RequestMapping(value="/readOnly", produces = "text/html")
	public @ResponseBody String readOnly(){
		SecurityContext context = SecurityContextHolder.getContext();
        if (context != null){
        	Authentication authentication = context.getAuthentication();
        	if (authentication != null){
        		for (GrantedAuthority auth : authentication.getAuthorities()) {
                    if (CEPATSConstants.WRITE_ACCESS.equals(auth.getAuthority())){
                    	return "false";
                    }
                }
        	}
        }
		return "true";
	}


//    private Map<String, String> getHeadersInfo() {
//
//        Map<String, String> map = new HashMap<>();
//
//        Enumeration headerNames = request.getHeaderNames();
//        while (headerNames.hasMoreElements()) {
//            String key = (String) headerNames.nextElement();
//            String value = request.getHeader(key);
//            map.put(key, value);
//        }
//
//        return map;
//    }
}