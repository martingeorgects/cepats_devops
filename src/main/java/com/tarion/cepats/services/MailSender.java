package com.tarion.cepats.services;

import com.tarion.cepats.services.exception.CepatsServiceException;

public interface MailSender {
	
    void sendEmailUsingEsp(String subject, String body, String fromAddress, String toAddresses) throws CepatsServiceException;
}
