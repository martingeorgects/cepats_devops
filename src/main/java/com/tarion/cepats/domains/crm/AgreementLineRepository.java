package com.tarion.cepats.domains.crm;

import com.tarion.cepats.domains.crm.entities.AgreementLineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;


/**
 * @author oagady
 * Oct 1, 2014
 */
@Repository
public interface AgreementLineRepository extends JpaRepository<AgreementLineEntity, Long> {
	
    @Query("SELECT item.agreementLineEndDate FROM AgreementLineEntity item WHERE item.enrolmentNumber = ?1")
    public AgreementLineEntity findAgreementLineEndDate(String enrolmentNumber);
    
    @Query("SELECT item.agreementLineEndDate FROM AgreementLineEntity item WHERE item.enrolmentNumber = ?1 and item.messageNBR = ?2 and item.messageSetNBR = ?3")
	public Timestamp findAgreementLineForEnrolment(String enrolmentNumber, Integer messageNBR, Integer messageSetNBR);
 
}
