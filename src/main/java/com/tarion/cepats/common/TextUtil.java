package com.tarion.cepats.common;

import java.util.List;

import com.tarion.cepats.web.dtos.ChangeDto;
import com.tarion.cepats.web.dtos.ItemHistoryDto;
import com.tarion.cepats.web.dtos.UpdateResultDto;

public class TextUtil {


	/**
	 * This method builds HTML table for email body for form update notifications
	 * 
	 * @param itemUpdateDtoList
	 * @return
	 * @throws Exception
	 */
	public static String buildHistoryHTML(List<ItemHistoryDto> itemUpdateDtoList)  throws Exception {
		StringBuffer builder = new StringBuffer("<table border=\"1\" style=\"padding-bottom:10px;max-width:850px;\">");
		builder.append("<thead><tr>");
		builder.append("<th style=\"white-space:nowrap\">Item #</th>");
		builder.append("<th>Changes</th>");
		builder.append("<th>Changes Made</th>");
		builder.append("<th style=\"white-space:nowrap\">Revised On</th>");
		builder.append("<th style=\"white-space:nowrap\">Revised By</th>");
		builder.append("</thead></tr>");
		builder.append("<tbody>");
		
		
		for (ItemHistoryDto itemUpdateDto : itemUpdateDtoList) {

			builder.append("<tr>");
			builder.append(
					"<td  rowspan=" + itemUpdateDto.getChanges().size()
							+ " style=\"vertical-align:text-top;text-align:center;white-space:nowrap\">").append(itemUpdateDto.getItemId()) .append("</td>");
			builder.append(buildChangesDetails(itemUpdateDto.getChanges().get(0)));
			builder.append("</tr>");
			for (int y = 1; y < itemUpdateDto.getChanges().size(); y++) {
				builder.append("<tr>");
				builder.append(buildChangesDetails(itemUpdateDto.getChanges().get(y)));
				builder.append("</tr>");
			}
		}

		builder.append("</tbody></table>");

		return builder.toString();
	}
	

    /**
     * This method builds upload error summary for email notification
     * @param invalidItems
     * @return
     */
    public static String generateUploadErrorsSummary(List<UpdateResultDto.InvalidItem> invalidItems) {

        Long startTime = LoggerUtil.logEnter(TextUtil.class, "debug", "generateUploadErrorsSummary", invalidItems.size());

        StringBuffer body = new StringBuffer("<table  border=\"1\">");
        body.append("<thead><tr>");
        body.append("<th>Row #</th>");
        body.append("<th>Error</th>");
        body.append("</thead></tr>");
        body.append("<tbody>");

        for (UpdateResultDto.InvalidItem invalidItem : invalidItems) {
            body.append("<tr>");
            body.append("<td>").append(invalidItem.getRowNumber()).append("</td>");
            body.append("<td>").append(invalidItem.getErrorMessage()).append("</td>");
            body.append("</tr>");
        }

        body.append("</tbody>");
        body.append("</table>");

        LoggerUtil.logExit(TextUtil.class, "debug", "generateUploadErrorsSummary", startTime);
        return body.toString();
    }

	/**
	 * @param changeDto
	 * @return
	 */
	private static String buildChangesDetails(ChangeDto changeDto) {
		StringBuilder builder = new StringBuilder();
		builder.append("<td style=\"vertical-align:text-top;white-space:nowrap\">").append(changeDto.getChangeName()).append("</td>");
		builder.append("<td style=\"vertical-align:text-top;max-width:500px;min-width:200px;\">").append(changeDto.getNewValue()).append("<br>")
				.append("<del>").append(changeDto.getOldValue()).append("</del>").append("</td>");
		builder.append("<td style=\"vertical-align:text-top;text-align:center;white-space:nowrap\">").append(changeDto.getRevisedOnDate()).append("<br>")
				.append(changeDto.getRevisedOnTime()).append("</td>");
		builder.append("<td style=\"vertical-align:text-top;text-align:center;white-space:nowrap\">").append(changeDto.getRevisedBy()).append("</td>");
		return builder.toString();
	}
}
