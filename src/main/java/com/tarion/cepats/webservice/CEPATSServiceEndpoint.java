package com.tarion.cepats.webservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import cepats.cepats.GetCEPATSListRequest;
import cepats.cepats.GetCEPATSListRequest.Enrolment;
import cepats.cepats.GetCEPATSListResponse;

import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.services.FormService;

@Endpoint
public class CEPATSServiceEndpoint {

	private static final String NAMESPACE_URI = "http://cepats/cepats.webservice";

	@Autowired
	FormService formService;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCEPATSListRequest")
	@ResponsePayload
	public GetCEPATSListResponse getCEPATSList( @RequestPayload GetCEPATSListRequest request) {
		long startTime = LoggerUtil.logEnter(CEPATSServiceEndpoint.class, "getCEPATSList", request);
		List< GetCEPATSListRequest.Enrolment> enrolmentList = request.getEnrolment();
		List<String> enrolments = new ArrayList<>();
		for(Enrolment enrolment : enrolmentList){
		   enrolments.add(enrolment.getEnrolmentNumber());
		}
		
		LoggerUtil .logEnter(CEPATSServiceEndpoint.class, "info", "getCEPATSList: ", enrolments);
		List<String> cepats = formService.getCEPATSForEnrolments(enrolments);
		GetCEPATSListResponse response = new GetCEPATSListResponse();
		response.getEnrolmentNumber().addAll(cepats);
		LoggerUtil.logExit(CEPATSServiceEndpoint.class, "getCEPATSList", enrolments, startTime);
		return response;
	}
}
