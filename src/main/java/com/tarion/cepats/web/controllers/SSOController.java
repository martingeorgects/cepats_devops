package com.tarion.cepats.web.controllers;

import java.sql.Timestamp;
import java.util.Calendar;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.tarion.cepats.common.CEPATSConstants;
import com.tarion.cepats.common.CEPATSUtil;
import com.tarion.cepats.common.LoggerUtil;
import com.tarion.cepats.domains.crm.entities.EnrolmentEntity;
import com.tarion.cepats.domains.cepats.entities.FormTypeEnum;
import com.tarion.cepats.domains.cepats.entities.SsoLoginEntity;
import com.tarion.cepats.domains.cepats.entities.UserTypeEnum;
import com.tarion.cepats.services.CRMService;
import com.tarion.cepats.services.FormService;
import com.tarion.cepats.services.SSOService;
import com.tarion.cepats.web.dtos.UserDto;

@Controller
public class SSOController {

	private static final String REDIRECT_LOGIN_FAILED = "loginFailed";
	private static final String REDIRECT_LOGIN = "login";

	@Autowired
	SSOService ssoService;
	@Autowired
	FormService formService;
	@Autowired
	private CRMService crmService;

	@RequestMapping(value="/monitor", method={RequestMethod.GET, RequestMethod.HEAD})
	public @ResponseBody
	String monitor(){
		return "CEPATS STATUS OK";
	}

	@RequestMapping(value = "/login/logUser/{token}", method = RequestMethod.GET, produces = "text/html")
	public String logUser(@PathVariable("token") String token, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		long startTime = LoggerUtil.logEnter(SSOController.class, "logUser", token);
		LoggerUtil.logClientIp(SSOController.class, "logUser", request); 

		try {
			SsoLoginEntity userEntity = ssoService.loadUserByToken(token);

			if (userEntity == null) {
				LoggerUtil.logExit(SSOController.class, "logUser", "Could not find token " + token, startTime);
				return REDIRECT_LOGIN_FAILED;
			}

			populateUserDto(userEntity, session, userEntity.getEnrolmentNumber());
			ssoService.logUser(userEntity);
			ssoService.deleteToken(token);
			
			UserDto userDto = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(userEntity.getEnrolmentNumber()));
			//add source system cookie
			String sourceSystem = null;
			if(userDto.getLoggedinUserType().compareTo(UserTypeEnum.VB) == 0){ 
				sourceSystem = "BSA"; 
			}else{
				sourceSystem = "HOP";
			}
			Cookie sourceSystemCookie = new Cookie("sourceSystem", sourceSystem);
			sourceSystemCookie.setMaxAge(24*60*60); //set expire time to 1 day
			sourceSystemCookie.setPath("/");
			response.addCookie(sourceSystemCookie); //put cookie in response
			
			LoggerUtil.logExit(SSOController.class, "logUser", CEPATSUtil.findMapping(userDto, userEntity.getEnrolmentNumber()), startTime);
			return CEPATSUtil.findMapping(userDto, userEntity.getEnrolmentNumber());
			
			
		} catch (Exception e) {
			LoggerUtil.logServiceError(SSOController.class, "logUser", e);
		}

		LoggerUtil.logExit(SSOController.class, "logUser", REDIRECT_LOGIN_FAILED, startTime);
		return REDIRECT_LOGIN_FAILED;
	}

	@RequestMapping(value = "/login/", produces = "text/html")
	public String getLoginPage(HttpServletRequest request, Model model,
			@CookieValue(value = "sourceSystem", defaultValue = "HOP") String sourceSystemCookieValue) {
		LoggerUtil.logClientIp(SSOController.class, "getLoginPage", request);
		
		if(sourceSystemCookieValue.equalsIgnoreCase("BSA")){
        	model.addAttribute("headerCssClass", "builderlinkHeader");
        }else{
        	model.addAttribute("headerCssClass", "myhomeHeader");
        }
		
		return REDIRECT_LOGIN;
	}

	@RequestMapping(value = "/loginFailed/", produces = "text/html")
	public String getLoginFailedPage(HttpServletRequest request, Model model,
			@CookieValue(value = "sourceSystem", defaultValue = "HOP") String sourceSystemCookieValue) {
		LoggerUtil.logClientIp(SSOController.class, "getLoginFailedPage", request);
		
		if(sourceSystemCookieValue.equalsIgnoreCase("BSA")){
        	model.addAttribute("headerCssClass", "builderlinkHeader");
        }else{
        	model.addAttribute("headerCssClass", "myhomeHeader");
        }
		
		return REDIRECT_LOGIN_FAILED;
	}

	private boolean logUserAndDeleteToken(String token, HttpSession session) throws Exception {
		long startTime = LoggerUtil.logEnter(SSOController.class, "logUserAndDeleteToken", token);
		SsoLoginEntity userEntity = ssoService.loadUserByToken(token);

		if (userEntity == null) {
			LoggerUtil.logExit(SSOController.class, "logUser", "Could not find token " + token, startTime);
			return false;
		}

		populateUserDto(userEntity, session, userEntity.getEnrolmentNumber());
		ssoService.logUser(userEntity);
		ssoService.deleteToken(token);

		LoggerUtil.logExit(SSOController.class, "logUser", true, startTime);
		return true;
	}

	private void populateUserDto(SsoLoginEntity user, HttpSession session, String enrolmentNumber) throws Exception {
		UserDto userFromSession = (UserDto)session.getAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber));
		UserDto userDto = null;
		if (userFromSession != null) {
			userDto = userFromSession;
		} else {
			userDto = new UserDto();
		}

		userDto.setUsername(user.getUserId());
		userDto.setFirstName(user.getUserFirstName());
		userDto.setLastName(user.getUserLastName());
		if(user.getUserType().equals(CEPATSConstants.MYHOME_CCA_READ_ONLY)){
			userDto.setLoggedinUserType(UserTypeEnum.CCA);
		}else{
			userDto.setLoggedinUserType(UserTypeEnum.valueOf(user.getUserType().toUpperCase()));
		}
		userDto.setEmailAddress(user.getEmailAddress());

		EnrolmentEntity enrolment = crmService.getEnrolment(user.getEnrolmentNumber());
		userDto.setEnrolment(enrolment);
		userDto.setFirstYearFormSubmitted(formService.isFormSubmitted(user.getEnrolmentNumber(), FormTypeEnum.FIRSTYEAR));
		userDto.setSecondYearFormSubmitted(formService.isFormSubmitted(user.getEnrolmentNumber(), FormTypeEnum.SECONDYEAR));
		userDto.setEndOfFirstYearWarranty(determineEndOfFirstYearWarranty(userDto));
		userDto.setEndOfSecondYearWarranty(determineEndOfSecondYearWarranty(userDto));
		
		session.setAttribute(CEPATSUtil.getUserSessionAttributeName(enrolmentNumber), userDto);

	}

	private Timestamp determineEndOfSecondYearWarranty(UserDto userDto) {
		Timestamp endOfAgreementLine 
			=  crmService.getAgreementLineEndDateSecondYear(userDto.getEnrolment().getEnrolmentNumber());
		return addOneDay(endOfAgreementLine);
	}

	private Timestamp determineEndOfFirstYearWarranty(UserDto userDto) {
		Timestamp endOfAgreementLine 
			=  crmService.getAgreementLineEndDateFirstYear(userDto.getEnrolment().getEnrolmentNumber());
		return addOneDay(endOfAgreementLine);
	}

	private Timestamp addOneDay(Timestamp endOfAgreementLine) {
		//Timestamp for the end date in CRM has 0 time portion (beginning of the day). 
		//In this case, the users will not be able to upload PATS on the last day of warranty.
		//Need to add one more day to make the last day available for uploading
		Calendar cal = Calendar.getInstance();
	    cal.setTimeInMillis(endOfAgreementLine.getTime());
	    cal.add(Calendar.DAY_OF_MONTH, 1);
	    endOfAgreementLine = new Timestamp(cal.getTime().getTime());
	    return endOfAgreementLine;
	}

}