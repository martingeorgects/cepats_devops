<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="mainContainer" class="col-xs-12">

	<div class="row">
		<hr/>
	</div>
	<div class="left">
		<h3>Frequently Asked Questions for Condominium Unit Enrolments</h3>
	</div>
	<div class="clear">&#160;</div>
	<div class="faqContainer left">
		<table class="table table-striped table-bordered">
			<tbody>
				 <c:forEach var="faq" items="${faqs}">
					<tr>
						<td><strong>${faq.contentNumber}. ${faq.contentText1}</strong></td>
					</tr>
					<tr>
						<td>${faq.contentText2}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div class="clear">&#160;</div>
	<div class="row">
		<hr/>
	</div>

 </div>
